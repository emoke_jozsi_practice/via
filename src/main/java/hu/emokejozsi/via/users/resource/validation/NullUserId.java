package hu.emokejozsi.via.users.resource.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import hu.emokejozsi.via.users.resource.model.ViaUser;

/**
 * Validation annotation to be used on a {@link ViaUser} parameter, indicating
 * that its id must be <code>null</code>.
 * 
 * @author Emoke_Borsi
 */
@Constraint(validatedBy = NullUserIdValidator.class)
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NullUserId {

    String message() default "Id must be null";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
