package hu.emokejozsi.via.users.resource.model;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Resource level model class containing information about the error which
 * occured during request processing.
 * 
 * @author Emoke_Borsi
 */
@ApiModel(
        value = "Error response body",
        description = "Contains information about the error")
@JsonDeserialize(builder = ViaErrorResponse.Builder.class)
public class ViaErrorResponse {

    @ApiModelProperty(position = 1)
    private final ViaError error;

    @ApiModelProperty(position = 2, value = "The URL path of the request")
    private final String path;

    @ApiModelProperty(position = 3, value = "The internal identifier of the request for tracking purposes")
    private final String guid;

    @ApiModelProperty(position = 4, value = "Additional information about the error")
    private final String details;

    @Generated("SparkTools")
    private ViaErrorResponse(Builder builder) {
        this.error = builder.error;
        this.path = builder.path;
        this.guid = builder.guid;
        this.details = builder.details;
    }

    public ViaError getError() {
        return error;
    }

    public String getPath() {
        return path;
    }

    public String getGuid() {
        return guid;
    }

    public String getDetails() {
        return details;
    }

    @Generated("Eclipse")
    @Override
    public String toString() {
        return "ViaErrorResponse [error=" + error + ", path=" + path + ", guid=" + guid + ", details=" + details + "]";
    }

    @Generated("Eclipse")
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((details == null) ? 0 : details.hashCode());
        result = prime * result + ((error == null) ? 0 : error.hashCode());
        result = prime * result + ((guid == null) ? 0 : guid.hashCode());
        result = prime * result + ((path == null) ? 0 : path.hashCode());
        return result;
    }

    @Generated("Eclipse")
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ViaErrorResponse other = (ViaErrorResponse) obj;
        if (details == null) {
            if (other.details != null)
                return false;
        } else if (!details.equals(other.details))
            return false;
        if (error != other.error)
            return false;
        if (guid == null) {
            if (other.guid != null)
                return false;
        } else if (!guid.equals(other.guid))
            return false;
        if (path == null) {
            if (other.path != null)
                return false;
        } else if (!path.equals(other.path))
            return false;
        return true;
    }

    @Generated("SparkTools")
    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder
    @Generated("SparkTools")
    public static final class Builder {
        private ViaError error;
        private String path;
        private String guid;
        private String details;

        private Builder() {
        }

        @Nonnull
        public Builder withError(@Nonnull ViaError error) {
            this.error = error;
            return this;
        }

        @Nonnull
        public Builder withPath(@Nonnull String path) {
            this.path = path;
            return this;
        }

        @Nonnull
        public Builder withGuid(@Nullable String guid) {
            this.guid = guid;
            return this;
        }

        @Nonnull
        public Builder withDetails(@Nullable String details) {
            this.details = details;
            return this;
        }

        @Nonnull
        public ViaErrorResponse build() {
            return new ViaErrorResponse(this);
        }
    }
}
