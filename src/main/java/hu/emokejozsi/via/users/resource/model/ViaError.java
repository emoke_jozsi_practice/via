package hu.emokejozsi.via.users.resource.model;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Enum class for typical errors of the application.
 * 
 * @author Emoke_Borsi
 */
@ApiModel(
        value = "Error types",
        description = "Enumeration of general error types")
@JsonFormat(shape = Shape.OBJECT)
public enum ViaError {

    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected server error"),
    USER_NOT_FOUND(HttpStatus.NOT_FOUND, "User not found"),
    INVALID_DATA(HttpStatus.BAD_REQUEST, "Invalid user input");

    @ApiModelProperty(position = 1)
    private final HttpStatus httpStatus;

    @ApiModelProperty(position = 2)
    private final String message;

    private ViaError(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }
}
