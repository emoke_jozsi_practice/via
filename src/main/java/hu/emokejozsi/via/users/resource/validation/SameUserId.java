package hu.emokejozsi.via.users.resource.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import hu.emokejozsi.via.users.resource.model.ViaUser;

/**
 * Validation annotation to be used on a method which has to parameters: a
 * {@link String} user id and a {@link ViaUser} (in this exact order), to
 * indicate that the id in the {@link ViaUser} must match the id passed as the
 * first parameter.
 * 
 * @author Emoke_Borsi
 */
@Constraint(validatedBy = SameUserIdValidator.class)
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SameUserId {

    String message() default "Ids must match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
