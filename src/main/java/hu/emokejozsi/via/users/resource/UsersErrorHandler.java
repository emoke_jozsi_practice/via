package hu.emokejozsi.via.users.resource;

import static java.util.stream.Collectors.joining;

import java.util.Objects;
import java.util.Set;

import javax.annotation.Nullable;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import hu.emokejozsi.via.config.TrackingFilter;
import hu.emokejozsi.via.users.resource.model.ViaError;
import hu.emokejozsi.via.users.resource.model.ViaErrorResponse;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

/**
 * An exception handler for all {@link RestController}s in the application.
 * 
 * @author Emoke_Borsi
 */
@RestControllerAdvice(value = "hu.emokejozsi.via")
public class UsersErrorHandler extends ResponseEntityExceptionHandler {

    private final Logger logger;

    /**
     * Creates a new {@link UsersErrorHandler} instance.
     * 
     * @param logger
     */
    public UsersErrorHandler(Logger logger) {
        this.logger = logger;
    }

    /**
     * Handles all unexpected {@link RuntimeException}s. Creates a
     * {@link ViaErrorResponse} containing details about the error, wrapped in a
     * {@link ResponseEntity}, with HTTP error code 500 and sensible HTTP response
     * headers.
     * 
     * @param request   - the {@link ServletWebRequest}
     * @param exception - the {@link RuntimeException}
     * @return the {@link ResponseEntity}
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleInternalServerError(ServletWebRequest request, RuntimeException exception) {
        logger.error(ViaError.INTERNAL_SERVER_ERROR.getMessage(), exception);

        return handleExceptionInternal(
                exception,
                createBody(request, ViaError.INTERNAL_SERVER_ERROR, null),
                createErrorHeaders(),
                ViaError.INTERNAL_SERVER_ERROR.getHttpStatus(),
                request);
    }

    /**
     * Handles all {@link UserNotFoundException}s. Creates a
     * {@link ViaErrorResponse} containing details about the error, wrapped in a
     * {@link ResponseEntity}, with HTTP error code 404 and sensible HTTP response
     * headers.
     * 
     * @param request   - the {@link ServletWebRequest}
     * @param exception - the {@link UserNotFoundException}
     * @return the {@link ResponseEntity}
     */
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Object> handleUserNotFound(ServletWebRequest request, UserNotFoundException exception) {
        long userId = exception.getUserId();

        logger.debug(ViaError.USER_NOT_FOUND.getMessage() + "; id: {}", exception, userId);

        return handleExceptionInternal(
                exception,
                createBody(request, ViaError.USER_NOT_FOUND, "id: " + userId),
                createErrorHeaders(),
                ViaError.USER_NOT_FOUND.getHttpStatus(), request);
    }

    /**
     * Handles all {@link ConstraintViolationException}s resulting from user input
     * validation. Creates a {@link ViaErrorResponse} containing details about the
     * error, wrapped in a {@link ResponseEntity}, with HTTP error code 400 and
     * sensible HTTP response headers.
     * 
     * @param request   - the {@link ServletWebRequest}
     * @param exception - the {@link ConstraintViolationException}
     * @return the {@link ResponseEntity}
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleValidationError(ServletWebRequest request,
            ConstraintViolationException exception) {
        logger.debug(ViaError.INVALID_DATA.getMessage(), exception);

        Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();
        String details = null;
        if (Objects.nonNull(constraintViolations)) {
            details = constraintViolations.stream().map(ConstraintViolation::getMessage).collect(joining(", "));
        } else {
            details = exception.getMessage();
        }

        return handleExceptionInternal(exception,
                createBody(request, ViaError.INVALID_DATA, details),
                createErrorHeaders(),
                ViaError.INVALID_DATA.getHttpStatus(),
                request);
    }

    /**
     * Handles all {@link MethodArgumentTypeMismatchException}s resulting from user
     * input validation. Creates a {@link ViaErrorResponse} containing details about
     * the error, wrapped in a {@link ResponseEntity}, with HTTP error code 400 and
     * sensible HTTP response headers
     * 
     * @param request   - the {@link ServletWebRequest}
     * @param exception - the {@link MethodArgumentTypeMismatchException}
     * @return the {@link ResponseEntity}
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(ServletWebRequest request,
            MethodArgumentTypeMismatchException exception) {
        logger.debug(ViaError.INVALID_DATA.getMessage(), exception);

        Class<?> requiredType = exception.getRequiredType();
        String details = exception.getName()
                + (Objects.nonNull(requiredType) ? " should be of type " + requiredType.getName() : "is invalid");

        return handleExceptionInternal(
                exception,
                createBody(request, ViaError.INVALID_DATA, details),
                createErrorHeaders(),
                ViaError.INVALID_DATA.getHttpStatus(),
                request);
    }

    /**
     * Handles all {@link MethodArgumentNotValidException}s resulting from user
     * input validation. Creates a {@link ViaErrorResponse} containing details about
     * the error, wrapped in a {@link ResponseEntity}, with HTTP error code 400 and
     * sensible HTTP response headers
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.debug(ViaError.INVALID_DATA.getMessage(), exception);

        String details = exception.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage)
                .collect(joining(", "));

        return handleExceptionInternal(
                exception,
                createBody((ServletWebRequest) request, ViaError.INVALID_DATA, details),
                createErrorHeaders(),
                ViaError.INVALID_DATA.getHttpStatus(),
                request);
    }

    private ViaErrorResponse createBody(ServletWebRequest request, ViaError viaError, @Nullable String details) {
        return ViaErrorResponse.builder()
                .withError(viaError)
                .withPath(request.getRequest().getRequestURI())
                .withGuid((String) request.getAttribute(TrackingFilter.GUID, RequestAttributes.SCOPE_REQUEST))
                .withDetails(details)
                .build();
    }

    private HttpHeaders createErrorHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.setCacheControl(CacheControl.noStore());
        return headers;
    }
}
