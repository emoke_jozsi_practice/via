package hu.emokejozsi.via.users.resource;

import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;

import hu.emokejozsi.via.users.facade.model.UserWithAdditionalInfo;
import hu.emokejozsi.via.users.resource.model.ViaUser;

/**
 * This class is responsible for converting between {@link ViaUser} and
 * {@link UserWithAdditionalInfo} objects.
 * 
 * @author Emoke_Borsi
 */
@Component
public class ViaUserConverter {

    /**
     * Creates a new {@link UserWithAdditionalInfo} object from the given
     * {@link ViaUser}.
     * 
     * @param viaUser - the {@link ViaUser} to convert
     * @return the new {@link UserWithAdditionalInfo}
     */
    @Nonnull
    public UserWithAdditionalInfo convertToUserWithAdditionalInfo(@Nonnull ViaUser viaUser) {
        UserWithAdditionalInfo user = UserWithAdditionalInfo.builder()
                .withId(viaUser.getId())
                .withName(viaUser.getName())
                .withUsername(viaUser.getUsername())
                .withDirectMarketingEnabled(viaUser.isDirectMarketingEnabled())
                .build();

        return user;
    }

    /**
     * Creates a new {@link ViaUser} object form the given
     * {@link UserWithAdditionalInfo}.
     * 
     * @param user - the {@link UserWithAdditionalInfo}
     * @return the new {@link ViaUser}
     */
    @Nonnull
    public ViaUser convertToViaUser(@Nonnull UserWithAdditionalInfo user) {
        ViaUser viaUser = ViaUser.builder()
                .withId(user.getId())
                .withName(user.getName())
                .withUsername(user.getUsername())
                .withDirectMarketingEnabled(user.isDirectMarketingEnabled())
                .build();

        return viaUser;
    }
}