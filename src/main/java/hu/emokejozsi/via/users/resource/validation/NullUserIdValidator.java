package hu.emokejozsi.via.users.resource.validation;

import java.util.Objects;

import javax.annotation.Nonnull;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import hu.emokejozsi.via.users.resource.model.ViaUser;

/**
 * This validator checks if a {@link ViaUser}'s id is <code>null</code> or not.
 * 
 * @author Emoke_Borsi
 */
public class NullUserIdValidator implements ConstraintValidator<NullUserId, ViaUser> {

    @Override
    public boolean isValid(@Nonnull ViaUser user, ConstraintValidatorContext context) {
        return Objects.isNull(user.getId());
    }
}
