package hu.emokejozsi.via.users.resource.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;

import hu.emokejozsi.via.users.resource.model.ViaUser;

/**
 * This validator checks in a 2-element array of a {@link String} user id and a
 * {@link ViaUser} (in this exact order) if the id in the {@link ViaUser}
 * matches the given {@link String}.
 * 
 * @author Emoke_Borsi
 */
@SupportedValidationTarget(ValidationTarget.PARAMETERS)
public class SameUserIdValidator implements ConstraintValidator<SameUserId, Object[]> {

    @Override
    public boolean isValid(Object[] value, ConstraintValidatorContext context) {
        if (!(value[0] instanceof String)
                || !(value[1] instanceof ViaUser)) {
            throw new IllegalArgumentException(
                    "Illegal method signature, expected two parameters of type String and ViaUser.");
        }

        String id = (String) value[0];
        Long viaUserId = ((ViaUser) value[1]).getId();

        return viaUserId != null && id.equals(viaUserId.toString());
        // we don't want to check the format, only the String equality
    }
}
