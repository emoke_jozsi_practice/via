package hu.emokejozsi.via.users.resource.model;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Resource level model class for user data.
 * 
 * @author Emoke_Borsi
 *
 */
@ApiModel(
        value = "VIA User",
        description = "Data structure containing user information")
@JsonDeserialize(builder = ViaUser.Builder.class)
public class ViaUser {

    @ApiModelProperty(position = 1, required = true)
    @Positive(message = "invalid user id")
    private final Long id;

    @ApiModelProperty(position = 2, required = true)
    @NotNull(message = "name is missing")
    private final String name;

    @ApiModelProperty(position = 3, required = true)
    @NotNull(message = "username is missing")
    private final String username;

    @ApiModelProperty(position = 4, required = false, notes = "Default is \"false\"")
    private final boolean directMarketingEnabled;

    @Generated("SparkTools")
    private ViaUser(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.username = builder.username;
        this.directMarketingEnabled = builder.directMarketingEnabled;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public boolean isDirectMarketingEnabled() {
        return directMarketingEnabled;
    }

    @Generated("Eclipse")
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (directMarketingEnabled ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Generated("Eclipse")
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ViaUser other = (ViaUser) obj;
        if (directMarketingEnabled != other.directMarketingEnabled)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

    @Generated("Eclipse")
    @Override
    public String toString() {
        return "ViaUser [id=" + id + ", name=" + name + ", username=" + username + ", directMarketingEnabled="
                + directMarketingEnabled + "]";
    }

    @Generated("SparkTools")
    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder
    @Generated("SparkTools")
    public static final class Builder {
        private Long id;
        private String name;
        private String username;
        private boolean directMarketingEnabled;

        private Builder() {
        }

        @Nonnull
        public Builder withId(@Nullable Long id) {
            this.id = id;
            return this;
        }

        @Nonnull
        public Builder withName(@Nonnull String name) {
            this.name = name;
            return this;
        }

        @Nonnull
        public Builder withUsername(@Nonnull String username) {
            this.username = username;
            return this;
        }

        @Nonnull
        public Builder withDirectMarketingEnabled(@Nonnull boolean directMarketingEnabled) {
            this.directMarketingEnabled = directMarketingEnabled;
            return this;
        }

        @Nonnull
        public ViaUser build() {
            return new ViaUser(this);
        }
    }

}
