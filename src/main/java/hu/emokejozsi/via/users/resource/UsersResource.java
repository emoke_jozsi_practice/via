package hu.emokejozsi.via.users.resource;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.slf4j.Logger;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import hu.emokejozsi.via.users.facade.UserFacade;
import hu.emokejozsi.via.users.facade.model.UserWithAdditionalInfo;
import hu.emokejozsi.via.users.resource.model.ViaErrorResponse;
import hu.emokejozsi.via.users.resource.model.ViaUser;
import hu.emokejozsi.via.users.resource.validation.NullUserId;
import hu.emokejozsi.via.users.resource.validation.SameUserId;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;
import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * {@link RestController} to offer REST GET, POST, PUT and DELETE endpoints to
 * the VIA user management application. Documentation is given by Swagger
 * annotations.
 * 
 * @author Emoke_Borsi
 */
@Api("/users")
@RestController
@Validated
@Timed
public class UsersResource {

    private final UserFacade userFacade;
    private final ViaUserConverter converter;
    private final Logger logger;

    public UsersResource(UserFacade userFacade, ViaUserConverter converter, Logger logger) {
        this.userFacade = userFacade;
        this.converter = converter;
        this.logger = logger;
    }

    @ApiOperation(value = "Lists all VIA users",
            notes = "Retrieves all users (from the backend user service and appends additional user data stored in the local database)",
            response = ViaUser[].class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ViaUser[].class),
            @ApiResponse(code = 500, message = "Internal server error", response = ViaErrorResponse.class)
    })
    @GetMapping("users")
    public ResponseEntity<List<ViaUser>> getAllUsers() {
        logger.info("Request: getAllUsers()");

        List<ViaUser> users = userFacade.getAllUsers().stream().map(converter::convertToViaUser).collect(toList());
        logger.debug("All users: {}", users);

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .cacheControl(CacheControl.maxAge(5, TimeUnit.MINUTES).mustRevalidate())
                .body(users);
    }

    @ApiOperation(value = "Retrieves a VIA user based on id",
            notes = "Retrieves a user with the given id (from the backend user service and appends additional user data stored in the local database)",
            response = ViaUser.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ViaUser.class),
            @ApiResponse(code = 404, message = "User not found", response = ViaErrorResponse.class),
            @ApiResponse(code = 400, message = "Invalid id format", response = ViaErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ViaErrorResponse.class)
    })
    @GetMapping("users/{id}")
    public ResponseEntity<ViaUser> getUser(
            @PathVariable @Pattern(regexp = "(\\d){1,9}", message = "Not a valid user id") String id)
            throws UserNotFoundException {
        logger.info("Request: getUser() id: {}", id);

        ViaUser user = converter.convertToViaUser(userFacade.getUser(Long.parseLong(id)));
        logger.debug("Retrieved user: {}", user);

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .cacheControl(CacheControl.maxAge(5, TimeUnit.MINUTES).mustRevalidate())
                .body(user);
    }

    @ApiOperation(value = "Creates a VIA user",
            notes = "Creates a new user (both in the backend user service and in the local database)",
            response = ViaUser.class)
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created", response = ViaUser.class),
            @ApiResponse(code = 404, message = "User not found", response = ViaErrorResponse.class),
            @ApiResponse(code = 400, message = "Invalid input", response = ViaErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ViaErrorResponse.class)
    })
    @PostMapping("users")
    public ResponseEntity<ViaUser> createUser(
            @RequestBody @Valid @NullUserId(message = "User id should be empty") ViaUser viaUser) {
        logger.info("Request: createUser: {}", viaUser);

        UserWithAdditionalInfo user = converter.convertToUserWithAdditionalInfo(viaUser);

        ViaUser createdUser = converter.convertToViaUser(userFacade.createUser(user));
        logger.debug("Created user: {}", createdUser);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .cacheControl(CacheControl.maxAge(5, TimeUnit.MINUTES).mustRevalidate())
                .body(createdUser);
    }

    @ApiOperation(value = "Updates a VIA user based on id",
            notes = "Updates the user with the given id (both in the backend user service and in the local database)",
            response = ViaUser.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = ViaUser.class),
            @ApiResponse(code = 404, message = "User not found", response = ViaErrorResponse.class),
            @ApiResponse(code = 400, message = "Invalid input", response = ViaErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ViaErrorResponse.class)
    })
    @PutMapping("users/{id}")
    @SameUserId(message = "Ids must match in the URL path and in the body")
    public ResponseEntity<ViaUser> updateUser(
            @PathVariable @Pattern(regexp = "(\\d){1,9}", message = "Not a valid user id") String id,
            @RequestBody @Valid ViaUser viaUser) throws UserNotFoundException {
        logger.info("Request: updateUser: id: {}, user data: {}", id, viaUser);

        UserWithAdditionalInfo user = converter.convertToUserWithAdditionalInfo(viaUser);

        ViaUser updatedUser = converter.convertToViaUser(userFacade.updateUser(user));
        logger.debug("Updated user: {}", updatedUser);

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .cacheControl(CacheControl.maxAge(5, TimeUnit.MINUTES).mustRevalidate())
                .body(updatedUser);
    }

    @ApiOperation(value = "Deletes a VIA user by id",
            notes = "Deletes the user with the given id (both in the backend user service and in the local database)",
            response = Void.class)
    @ApiResponses({
            @ApiResponse(code = 204, message = "Success - No content", response = Void.class),
            @ApiResponse(code = 400, message = "Invalid id format", response = ViaErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ViaErrorResponse.class)
    })
    @DeleteMapping("users/{id}")
    public ResponseEntity<Void> deleteUser(
            @PathVariable @Pattern(regexp = "(\\d){1,9}", message = "Not a valid user id") String id) {
        logger.info("Request: deleteUser() id: {}", id);

        userFacade.deleteUser(Long.parseLong(id));
        logger.debug("Deleted user with id: {}", id);

        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body(null);
    }
}
