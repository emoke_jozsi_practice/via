package hu.emokejozsi.via.users.facade.model;

import javax.annotation.Generated;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * Facade level model class for a user. Contains all data from both the external
 * user service and the local database.
 * 
 * @author Emoke_Borsi
 */
@JsonDeserialize(builder = UserWithAdditionalInfo.Builder.class)
public class UserWithAdditionalInfo {

    private final Long id;
    private final String name;
    private final String username;
    private final boolean directMarketingEnabled;

    @Generated("SparkTools")
    private UserWithAdditionalInfo(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.username = builder.username;
        this.directMarketingEnabled = builder.directMarketingEnabled;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public boolean isDirectMarketingEnabled() {
        return directMarketingEnabled;
    }

    @Generated("Eclipse")
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserWithAdditionalInfo other = (UserWithAdditionalInfo) obj;
        if (directMarketingEnabled != other.directMarketingEnabled)
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

    @Generated("Eclipse")
    @Override
    public String toString() {
        return "User [id=" + id + ", name=" + name + ", username=" + username + ", directMarketingEnabled="
                + directMarketingEnabled + "]";
    }

    @Generated("Eclipse")
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (directMarketingEnabled ? 1231 : 1237);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Generated("SparkTools")
    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder
    @Generated("SparkTools")
    public static final class Builder {
        private Long id;
        private String name;
        private String username;
        private boolean directMarketingEnabled;

        private Builder() {
        }

        @Nonnull
        public Builder withId(@Nonnull Long id) {
            this.id = id;
            return this;
        }

        @Nonnull
        public Builder withName(@Nonnull String name) {
            this.name = name;
            return this;
        }

        @Nonnull
        public Builder withUsername(@Nonnull String username) {
            this.username = username;
            return this;
        }

        @Nonnull
        public Builder withDirectMarketingEnabled(@Nonnull boolean directMarketingEnabled) {
            this.directMarketingEnabled = directMarketingEnabled;
            return this;
        }

        @Nonnull
        public UserWithAdditionalInfo build() {
            return new UserWithAdditionalInfo(this);
        }
    }
}
