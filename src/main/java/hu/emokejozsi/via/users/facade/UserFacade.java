package hu.emokejozsi.via.users.facade;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;

import hu.emokejozsi.via.users.dao.model.UserAdditionalInfo;
import hu.emokejozsi.via.users.facade.model.UserWithAdditionalInfo;
import hu.emokejozsi.via.users.service.AdditionalInfoService;
import hu.emokejozsi.via.users.service.JsonPlaceholderUserService;
import hu.emokejozsi.via.users.service.model.AdditionalInfo;
import hu.emokejozsi.via.users.service.model.User;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

/**
 * A Facade class for retrieving and manipulating user data. Serves as a single
 * entry point to the underlying services and database.
 * 
 * @author Emoke_Borsi
 */
@Component
public class UserFacade {

    private final JsonPlaceholderUserService userService;
    private final AdditionalInfoService additionalInfoService;
    private final UserWithAdditionalInfoConverter converter;

    /**
     * Creates a new instance.
     * 
     * @param userService
     * @param additionalInfoService
     * @param converter
     */
    public UserFacade(JsonPlaceholderUserService userService, AdditionalInfoService additionalInfoService,
            UserWithAdditionalInfoConverter converter) {
        this.userService = userService;
        this.additionalInfoService = additionalInfoService;
        this.converter = converter;
    }

    /**
     * Retrieves the list of all users. Combines data from the local database and
     * the external user service.
     * 
     * @return a {@link List} of all users
     */
    @Nonnull
    public List<UserWithAdditionalInfo> getAllUsers() {
        List<User> users = userService.getUsers();

        Map<Long, AdditionalInfo> additionalInfo = additionalInfoService.getAdditionalInfos().stream()
                .collect(toMap(AdditionalInfo::getUserId, Function.identity()));

        return users.stream()
                .map(user -> converter.convertToUserWithAdditionalInfo(user, additionalInfo.get(user.getId())))
                .collect(toList());
    }

    /**
     * Returns the user with the given id, or throws {@link UserNotFoundException}
     * if not found. Combines user data from the local database and the external
     * user service.
     * 
     * @param id - the ID of the user
     * @return the {@link UserAdditionalInfo} for the user
     * @throws UserNotFoundException - when there is no user found with the given id
     */
    @Nonnull
    public UserWithAdditionalInfo getUser(long id) throws UserNotFoundException {
        User user = userService.getUser(id);
        AdditionalInfo info = additionalInfoService.getAdditionalInfo(id).orElse(null);

        return converter.convertToUserWithAdditionalInfo(user, info);
    }

    /**
     * Stores the user in the external user service and in the local database.
     * {@link UserWithAdditionalInfo#id} needs to be null.
     * 
     * @param userWithAdditionalInfo - the user to create
     * @return the created user
     */
    @Nonnull
    public UserWithAdditionalInfo createUser(@Nonnull UserWithAdditionalInfo userWithAdditionalInfo) {
        User createdUser = userService.createUser(converter.convertToUser(userWithAdditionalInfo));

        AdditionalInfo additionalInfo = converter.convertToAdditionalInfoWithNewId(userWithAdditionalInfo,
                createdUser.getId());
        additionalInfoService.saveAdditionalInfo(additionalInfo);

        return converter.convertToUserWithAdditionalInfo(createdUser, additionalInfo);
    }

    /**
     * Updates the existing user in the external service and in the local database.
     * Throws {@link UserWithAdditionalInfo}
     * 
     * @param user - the user to update.
     * @return the updated user
     * @throws UserNotFoundException - when there is no user with the given
     *                               {@link UserWithAdditionalInfo#id}
     */
    @Nonnull
    public UserWithAdditionalInfo updateUser(@Nonnull UserWithAdditionalInfo user) throws UserNotFoundException {
        userService.updateUser(converter.convertToUser(user));
        additionalInfoService.saveAdditionalInfo(converter.convertToAdditionalInfo(user));
        return user;
    }

    /**
     * Deletes the user with the given id. This operation is idempotent, throws no
     * exception when there is no such user.
     * 
     * @param id - the id of the user to delete
     */
    public void deleteUser(long id) {
        additionalInfoService.deleteAdditionalInfo(id);
        userService.deleteUser(id);
    }
}
