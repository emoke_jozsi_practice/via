package hu.emokejozsi.via.users.facade;

import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.stereotype.Component;

import hu.emokejozsi.via.users.facade.model.UserWithAdditionalInfo;
import hu.emokejozsi.via.users.service.model.AdditionalInfo;
import hu.emokejozsi.via.users.service.model.User;

/**
 * This class is responsible for converting between
 * {@link UserWithAdditionalInfo} and {@link User} plus {@link AdditionalInfo}
 * objects.
 * 
 * @author Emoke_Borsi
 */
@Component
public class UserWithAdditionalInfoConverter {

    /**
     * Creates a {@link UserWithAdditionalInfo} object from the {@link User}
     * and @{link {@link AdditionalInfo}.
     * 
     * @param user           - the {@link User} object
     * @param additionalInfo - the {@link AdditionalInfo} object; can be null
     * @return
     */
    @Nonnull
    public UserWithAdditionalInfo convertToUserWithAdditionalInfo(@Nonnull User user,
            @Nullable AdditionalInfo additionalInfo) {
        return UserWithAdditionalInfo.builder()
                .withId(user.getId())
                .withName(user.getName())
                .withUsername(user.getUsername())
                .withDirectMarketingEnabled(Optional.ofNullable(additionalInfo)
                        .map(AdditionalInfo::isDirectMarketingEnabled)
                        .orElse(false))
                .build();
    }

    /**
     * Creates a {@link User} from the given {@link UserWithAdditionalInfo} object.
     * 
     * @param userWithAdditionalInfo - the {@link UserWithAdditionalInfo} to convert
     * @return the new {@link User} object
     */
    @Nonnull
    public User convertToUser(@Nonnull UserWithAdditionalInfo userWithAdditionalInfo) {
        return User.builder()
                .withId(userWithAdditionalInfo.getId())
                .withName(userWithAdditionalInfo.getName())
                .withUsername(userWithAdditionalInfo.getUsername())
                .build();
    }

    /**
     * Creates and {@link AdditionalInfo} object from the given
     * {@link UserWithAdditionalInfo} object.
     * 
     * @param userWithAdditionalInfo - the {@link UserWithAdditionalInfo} to convert
     * @return the new {@link AdditionalInfo} object
     */
    @Nonnull
    public AdditionalInfo convertToAdditionalInfo(@Nonnull UserWithAdditionalInfo userWithAdditionalInfo) {
        return AdditionalInfo.builder()
                .withUserId(userWithAdditionalInfo.getId())
                .withDirectMarketingEnabled(userWithAdditionalInfo.isDirectMarketingEnabled())
                .build();
    }

    /**
     * Creates an {@link AdditionalInfo} from the {@link UserWithAdditionalInfo}
     * object and sets its id to the newId parameter.
     * 
     * @param user  - the {@link UserWithAdditionalInfo} to convert
     * @param newId - the id to set
     * @return the new {@link AdditionalInfo} object
     */
    @Nonnull
    public AdditionalInfo convertToAdditionalInfoWithNewId(@Nonnull UserWithAdditionalInfo user, @Nonnull Long newId) {
        return AdditionalInfo.builder()
                .withUserId(newId)
                .withDirectMarketingEnabled(user.isDirectMarketingEnabled())
                .build();
    }
}