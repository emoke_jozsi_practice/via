package hu.emokejozsi.via.users.dao;

import org.springframework.data.repository.CrudRepository;

import hu.emokejozsi.via.users.dao.model.UserAdditionalInfo;

/**
 * <code>CrudRepository</code> interface for managing {@link UserAdditionalInfo}
 * entities. *
 * 
 * @author Emoke_Borsi
 */
public interface UserAdditionalInfoRepository extends CrudRepository<UserAdditionalInfo, Long> {

}
