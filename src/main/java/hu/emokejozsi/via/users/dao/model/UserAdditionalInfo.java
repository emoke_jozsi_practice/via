package hu.emokejozsi.via.users.dao.model;

import javax.annotation.Generated;
import javax.annotation.Nonnull;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * Entity class for additional user information. Its ID comes from the external
 * user service
 * 
 * @author Emoke_Borsi
 */
@JsonDeserialize(builder = UserAdditionalInfo.Builder.class)
@Entity
public class UserAdditionalInfo {

    @Id
    private Long userId;

    private boolean directMarketingEnabled;

    public UserAdditionalInfo() {
    }

    @Generated("SparkTools")
    private UserAdditionalInfo(Builder builder) {
        this.userId = builder.userId;
        this.directMarketingEnabled = builder.directMarketingEnabled;
    }

    public Long getUserId() {
        return userId;
    }

    public boolean isDirectMarketingEnabled() {
        return directMarketingEnabled;
    }

    @Generated("SparkTools")
    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder
    @Generated("SparkTools")
    public static final class Builder {
        private Long userId;
        private boolean directMarketingEnabled;

        private Builder() {
        }

        @Nonnull
        public Builder withUserId(@Nonnull Long userId) {
            this.userId = userId;
            return this;
        }

        @Nonnull
        public Builder withDirectMarketingEnabled(@Nonnull boolean directMarketingEnabled) {
            this.directMarketingEnabled = directMarketingEnabled;
            return this;
        }

        @Nonnull
        public UserAdditionalInfo build() {
            return new UserAdditionalInfo(this);
        }
    }
}
