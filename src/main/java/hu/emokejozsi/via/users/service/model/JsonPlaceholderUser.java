package hu.emokejozsi.via.users.service.model;

import javax.annotation.Generated;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * Client service level model class for user data of the external user service.
 * 
 * @author Emoke_Borsi
 */
@JsonDeserialize(builder = JsonPlaceholderUser.Builder.class)
public class JsonPlaceholderUser {

    private final Long id;
    private final String name;
    private final String username;

    @Generated("SparkTools")
    private JsonPlaceholderUser(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.username = builder.username;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JsonPlaceholderUser other = (JsonPlaceholderUser) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "JsonPlaceholderUser [id=" + id + ", name=" + name + ", username=" + username + "]";
    }

    @Generated("SparkTools")
    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder
    @Generated("SparkTools")
    public static final class Builder {
        private Long id;
        private String name;
        private String username;

        private Builder() {
        }

        @Nonnull
        public Builder withId(@Nonnull Long id) {
            this.id = id;
            return this;
        }

        @Nonnull
        public Builder withName(@Nonnull String name) {
            this.name = name;
            return this;
        }

        @Nonnull
        public Builder withUsername(@Nonnull String username) {
            this.username = username;
            return this;
        }

        @Nonnull
        public JsonPlaceholderUser build() {
            return new JsonPlaceholderUser(this);
        }
    }
}
