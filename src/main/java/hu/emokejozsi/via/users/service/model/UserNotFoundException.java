package hu.emokejozsi.via.users.service.model;

import javax.annotation.Generated;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * An {@link Exception} subclass for signaling that there is no such user with
 * the given user id.
 * 
 * @author Emoke_Borsi
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class UserNotFoundException extends Exception {

    @Generated("Eclipse")
    private static final long serialVersionUID = 1125745494145750048L;

    private final long userId;

    public UserNotFoundException(long userId, String message) {
        super(message);
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }
}
