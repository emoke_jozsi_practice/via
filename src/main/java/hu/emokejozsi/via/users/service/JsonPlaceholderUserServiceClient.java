package hu.emokejozsi.via.users.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import hu.emokejozsi.via.users.service.model.JsonPlaceholderUser;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

/**
 * This class is responsible for accessing the external user service. Methods in
 * this class throw {@link HttpClientErrorException} when a communication error
 * occurs.
 * 
 * @author Emoke_Borsi
 */
@Component
public class JsonPlaceholderUserServiceClient {

    private final RestTemplate restTemplate;

    private final String url;

    private final Logger logger;

    /**
     * Creates a new {@link JsonPlaceholderUserServiceClient} instance.
     * 
     * @param restTemplate
     * @param url
     * @param logger
     */
    public JsonPlaceholderUserServiceClient(RestTemplate restTemplate,
            @Value("${hu.emokejozsi.via.users.url}") String url, Logger logger) {
        this.restTemplate = restTemplate;
        this.url = url;
        this.logger = logger;
    }

    /**
     * Retrieves the list of all users in the external user service.
     * 
     * @return the {@link List} of {@link JsonPlaceholderUser}s; may be an empty
     *         list
     */
    @HystrixCommand(fallbackMethod = "getUsersFallback")
    @Nonnull
    public List<JsonPlaceholderUser> getUsers() {
        JsonPlaceholderUser[] result = restTemplate.getForObject(url, JsonPlaceholderUser[].class);
        return Optional.ofNullable(result).map(Arrays::asList).orElse(Collections.emptyList());
    }

    /**
     * Retrieves the {@link JsonPlaceholderUser} with the given id from the external
     * user service.
     * 
     * @param id - the user id
     * @return the {@link JsonPlaceholderUser}
     * @throws UserNotFoundException if there is no user in the external user
     *                               service with the given id
     */
    @HystrixCommand(
            ignoreExceptions = UserNotFoundException.class,
            fallbackMethod = "getUserFallback")
    @Nonnull
    public JsonPlaceholderUser getUser(long id) throws UserNotFoundException {
        try {
            JsonPlaceholderUser result = restTemplate.getForObject(url + "/{id}", JsonPlaceholderUser.class, id);
            // this should not happen, but let's prepare for it
            return Optional.ofNullable(result)
                    .orElseThrow(() -> new UserNotFoundException(id, "User not found with id: " + id));
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new UserNotFoundException(id, "User not found with id: " + id);
            } else {
                throw e;
            }
        }
    }

    /**
     * Initiates the creation of a new user in the external service. The user's id
     * must be null otherwise 500 response will be returned.
     * 
     * @param user the {@link JsonPlaceholderUser} to create
     * @return the created {@link JsonPlaceholderUser} with the new user id set
     */
    @HystrixCommand(fallbackMethod = "createUserFallback")
    @Nullable
    public JsonPlaceholderUser createUser(@Nonnull JsonPlaceholderUser user) {
        return restTemplate.postForObject(url, user, JsonPlaceholderUser.class);
    }

    /**
     * Updates the user with the same id in the external user service.
     * 
     * @param user the {@link JsonPlaceholderUser} to update
     * @throws UserNotFoundException if there is no user in the external user
     *                               service with the given id
     */
    @HystrixCommand(
            ignoreExceptions = UserNotFoundException.class,
            fallbackMethod = "updateUserFallback")
    public void updateUser(@Nonnull JsonPlaceholderUser user) throws UserNotFoundException {
        try {
            restTemplate.put(url + "/{id}", user, user.getId());
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new UserNotFoundException(user.getId(), "User not found with id: " + user.getId());
            } else {
                throw e;
            }
        }
    }

    /**
     * Deletes the user from the external user service. Throws no exception when
     * there is no such user.
     * 
     * @param id - the user id
     */
    @HystrixCommand(fallbackMethod = "deleteUserFallback")
    public void deleteUser(long id) {
        try {
            restTemplate.delete(url + "/{id}", id);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                logger.info("deleteUser: no user found with id: {}", id);
            } else {
                throw e;
            }
        }
    }

    @SuppressWarnings("unused") // used by HystrixCommand
    private List<JsonPlaceholderUser> getUsersFallback() {
        throw createRemoteError();
    }

    @SuppressWarnings("unused") // used by HystrixCommand
    private JsonPlaceholderUser getUserFallback(long id) {
        throw createRemoteError();
    }

    @SuppressWarnings("unused") // used by HystrixCommand
    private JsonPlaceholderUser createUserFallback(JsonPlaceholderUser user) {
        throw createRemoteError();
    }

    @SuppressWarnings("unused") // used by HystrixCommand
    private void updateUserFallback(JsonPlaceholderUser user) {
        throw createRemoteError();
    }

    @SuppressWarnings("unused") // used by HystrixCommand
    private void deleteUserFallback(long id) {
        throw createRemoteError();
    }

    private RuntimeException createRemoteError() {
        return new IllegalStateException("Unexpected server error");
    }
}
