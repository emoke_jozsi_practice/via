package hu.emokejozsi.via.users.service;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.stereotype.Service;

import hu.emokejozsi.via.users.service.model.User;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

/**
 * Service class for retrieving and manipulating user data in the external user
 * service.
 * 
 * @author Emoke_Borsi
 */
@Service
public class JsonPlaceholderUserService {

    private final JsonPlaceholderUserServiceClient userClient;
    private final JsonPlaceholderUserConverter converter;

    public JsonPlaceholderUserService(JsonPlaceholderUserServiceClient userClient,
            JsonPlaceholderUserConverter converter) {
        this.userClient = userClient;
        this.converter = converter;
    }

    /**
     * Retrieves the list of all users in the external user service.
     * 
     * @return the {@link List} of users
     */
    @Nonnull
    public List<User> getUsers() {
        return userClient.getUsers().stream().map(converter::convertToUser).collect(toList());
    }

    /**
     * Retrieves the user with the given user id from the external service. Throws
     * {@link UserNotFoundException} if no user is found with the given id.
     * 
     * @param id - the user id
     * @return the retrieved {@link User}
     * @throws UserNotFoundException if there is no such user in the external
     *                               service
     */
    @Nonnull
    public User getUser(long id) throws UserNotFoundException {
        return converter.convertToUser(userClient.getUser(id));
    }

    /**
     * Creates a new user. The {@link User#id} needs to be null otherwise the
     * external user service will return 500.
     * 
     * @param user the {@link User} to create
     * @return the created {@link User} with the id set properly
     */
    @Nonnull
    public User createUser(@Nonnull User user) {
        return converter.convertToUser(userClient.createUser(converter.convertToJsonPlaceholderUser(user)));
    }

    /**
     * Updates an existing user with the user id {@link User#id}. Throws
     * {@link UserNotFoundException} if no user is found with the given id.
     * 
     * @param user - the {@link User} to update
     * @return the updated {@link User}
     * @throws UserNotFoundException if there is no such user in the external
     *                               service
     */
    @Nonnull
    public User updateUser(@Nonnull User user) throws UserNotFoundException {
        userClient.updateUser(converter.convertToJsonPlaceholderUser(user));
        return user;
    }

    /**
     * Deletes the {@link User} with the given id. Throws no exception when there is
     * no such user.
     * 
     * @param id - the user id
     */
    public void deleteUser(long id) {
        userClient.deleteUser(id);
    }
}
