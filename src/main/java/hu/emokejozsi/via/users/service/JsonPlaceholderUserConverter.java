package hu.emokejozsi.via.users.service;

import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;

import hu.emokejozsi.via.users.service.model.JsonPlaceholderUser;
import hu.emokejozsi.via.users.service.model.User;

/**
 * This class is responsible for converting between {@link JsonPlaceholderUser}
 * and {@link User} instances.
 * 
 * @author Emoke_Borsi
 */
@Component
public class JsonPlaceholderUserConverter {

    /**
     * Creates a {@link User} from the given {@link JsonPlaceholderUser} object.
     * 
     * @param jsonPlaceholderUser - the {@link JsonPlaceholderUser} to convert
     * @return the {@link User}
     */
    @Nonnull
    public User convertToUser(@Nonnull JsonPlaceholderUser jsonPlaceholderUser) {
        return User.builder()
                .withId(jsonPlaceholderUser.getId())
                .withName(jsonPlaceholderUser.getName())
                .withUsername(jsonPlaceholderUser.getUsername())
                .build();
    }

    /**
     * Creates a {@link JsonPlaceholderUser} from the given {@link User} object.
     * 
     * @param user - the {@link User} to convert
     * @return the {@link JsonPlaceholderUser}
     */
    @Nonnull
    public JsonPlaceholderUser convertToJsonPlaceholderUser(@Nonnull User user) {
        return JsonPlaceholderUser.builder()
                .withId(user.getId())
                .withName(user.getName())
                .withUsername(user.getUsername())
                .build();
    }
}