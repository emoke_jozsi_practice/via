package hu.emokejozsi.via.users.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import hu.emokejozsi.via.users.dao.UserAdditionalInfoRepository;
import hu.emokejozsi.via.users.dao.model.UserAdditionalInfo;
import hu.emokejozsi.via.users.service.model.AdditionalInfo;

/**
 * Service class for retrieving and manipulating additional user data stored
 * locally and not retrieved from the external user service.
 * 
 * @author Emoke_Borsi
 */
@Service
public class AdditionalInfoService {

    private final UserAdditionalInfoConverter converter;
    private final UserAdditionalInfoRepository userAdditionalInfoRepository;
    private final Logger logger;

    /**
     * Creates a new {@link AdditionalInfoService}.
     * 
     * @param converter
     * @param userAdditionalInfoRepository
     * @param logger
     */
    public AdditionalInfoService(UserAdditionalInfoConverter converter,
            UserAdditionalInfoRepository userAdditionalInfoRepository, Logger logger) {
        this.converter = converter;
        this.userAdditionalInfoRepository = userAdditionalInfoRepository;
        this.logger = logger;
    }

    /**
     * Retrieves the list of all additional user data.
     * 
     * @return a {@link List} of all existing {@link AdditionalInfo}
     */
    @Nonnull
    public List<AdditionalInfo> getAdditionalInfos() {
        Iterable<UserAdditionalInfo> iterableAdditionalInfo = userAdditionalInfoRepository.findAll();
        return StreamSupport.stream(iterableAdditionalInfo.spliterator(), false)
                .map(converter::convertToAdditionalInfo).collect(toList());
    }

    /**
     * Retrieves an {@link Optional} which contains the {@link AdditionalInfo} with
     * the given id if there is one. No exception is thrown when there is no data
     * stored for the given id in the local database as it is a valid case for
     * users.
     * 
     * @param id - the user id
     * @return the retrieved {@link AdditionalInfo} wrapped in an {@link Optional}
     */
    @Nonnull
    public Optional<AdditionalInfo> getAdditionalInfo(long id) {
        return userAdditionalInfoRepository.findById(id).map(converter::convertToAdditionalInfo);
    }

    /**
     * Creates or updates the given {@link AdditionalInfo}.
     * 
     * @param additionalInfo - {@link AdditionalInfo} to save
     */
    public void saveAdditionalInfo(@Nonnull AdditionalInfo additionalInfo) {
        userAdditionalInfoRepository.save(converter.convertToUserAdditionalInfo(additionalInfo));
    }

    /**
     * Deletes the {@link AdditionalInfo} with the given user id. No exception is
     * thrown when there is no such addition user data.
     * 
     * @param id - the user id
     */
    public void deleteAdditionalInfo(long id) {
        try {
            userAdditionalInfoRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            logger.info("deleteUser: No user found in local DB with id: {}", id);
        }
    }
}
