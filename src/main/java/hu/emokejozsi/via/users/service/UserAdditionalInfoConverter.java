package hu.emokejozsi.via.users.service;

import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;

import hu.emokejozsi.via.users.dao.model.UserAdditionalInfo;
import hu.emokejozsi.via.users.service.model.AdditionalInfo;

/**
 * This class is responsible for converting between {@link UserAdditionalInfo}
 * and {@link AdditionalInfo} instances.
 * 
 * @author Emoke_Borsi
 */
@Component
public class UserAdditionalInfoConverter {

    /**
     * Creates a new {@link AdditionalInfo} from the given
     * {@link UserAdditionalInfo} object.
     * 
     * @param userAdditionalInfo - the {@link UserAdditionalInfo} to convert
     * @return the new {@link AdditionalInfo} instance
     */
    @Nonnull
    public AdditionalInfo convertToAdditionalInfo(@Nonnull UserAdditionalInfo userAdditionalInfo) {
        return AdditionalInfo.builder()
                .withUserId(userAdditionalInfo.getUserId())
                .withDirectMarketingEnabled(userAdditionalInfo.isDirectMarketingEnabled())
                .build();
    }

    /**
     * Creates a new {@link UserAdditionalInfo} from the given
     * {@link AdditionalInfo} object.
     * 
     * @param additionalInfo - the {@link AdditionalInfo} to convert
     * @return the new {@link UserAdditionalInfo} instance
     */
    @Nonnull
    public UserAdditionalInfo convertToUserAdditionalInfo(@Nonnull AdditionalInfo additionalInfo) {
        return UserAdditionalInfo.builder()
                .withUserId(additionalInfo.getUserId())
                .withDirectMarketingEnabled(additionalInfo.isDirectMarketingEnabled())
                .build();
    }
}