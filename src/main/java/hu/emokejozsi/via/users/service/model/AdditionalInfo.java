package hu.emokejozsi.via.users.service.model;

import javax.annotation.Generated;
import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * Service level model class for additional user information stored locally.
 * 
 * @author Emoke_Borsi
 */
@JsonDeserialize(builder = AdditionalInfo.Builder.class)
public class AdditionalInfo {

    private final Long userId;
    private final boolean directMarketingEnabled;

    @Generated("SparkTools")
    private AdditionalInfo(Builder builder) {
        this.userId = builder.userId;
        this.directMarketingEnabled = builder.directMarketingEnabled;
    }

    public Long getUserId() {
        return userId;
    }

    public boolean isDirectMarketingEnabled() {
        return directMarketingEnabled;
    }

    @Generated("Eclipse")
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (directMarketingEnabled ? 1231 : 1237);
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        return result;
    }

    @Generated("Eclipse")
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AdditionalInfo other = (AdditionalInfo) obj;
        if (directMarketingEnabled != other.directMarketingEnabled)
            return false;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        return true;
    }

    @Generated("Eclipse")
    @Override
    public String toString() {
        return "AdditionalInfo [userId=" + userId + ", directMarketingEnabled=" + directMarketingEnabled + "]";
    }

    @Generated("SparkTools")
    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder
    @Generated("SparkTools")
    public static final class Builder {
        private Long userId;
        private boolean directMarketingEnabled;

        private Builder() {
        }

        @Nonnull
        public Builder withUserId(@Nonnull Long userId) {
            this.userId = userId;
            return this;
        }

        @Nonnull
        public Builder withDirectMarketingEnabled(@Nonnull boolean directMarketingEnabled) {
            this.directMarketingEnabled = directMarketingEnabled;
            return this;
        }

        @Nonnull
        public AdditionalInfo build() {
            return new AdditionalInfo(this);
        }
    }
}
