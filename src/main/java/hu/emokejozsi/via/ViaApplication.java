package hu.emokejozsi.via;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point of the application.
 * 
 * @author Emoke_Borsi
 */
@SpringBootApplication
public class ViaApplication {

    public static void main(String[] args) {
        SpringApplication.run(ViaApplication.class, args);
    }
}
