package hu.emokejozsi.via.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger related configuration.
 * 
 * @author Emoke_Borsi
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .description("VIA User management API")
                        .version("0.1")
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("hu.emokejozsi.via.users.resource"))
                .paths(PathSelectors.any())
                .build();
    }
}
