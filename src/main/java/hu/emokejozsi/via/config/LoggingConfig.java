package hu.emokejozsi.via.config;

import static java.util.Objects.nonNull;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Tracking related configuration.
 * 
 * @author Emoke_Borsi
 *
 */
@Configuration
public class LoggingConfig {

    @Bean
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public Logger slf4jLogger(@Nonnull InjectionPoint injectionPoint) {
        return LoggerFactory.getLogger(getClassForInjectionPoint(injectionPoint));
    }

    private Class<?> getClassForInjectionPoint(@Nonnull InjectionPoint injectionPoint) {
        Class<?> enclosingClass = null;

        if (nonNull(injectionPoint.getMethodParameter())) {
            enclosingClass = injectionPoint.getMethodParameter().getContainingClass();
        } else if (nonNull(injectionPoint.getField())) {
            enclosingClass = injectionPoint.getField().getDeclaringClass();
        }

        return enclosingClass;
    }
}
