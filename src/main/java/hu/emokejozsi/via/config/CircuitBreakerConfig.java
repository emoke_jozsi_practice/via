package hu.emokejozsi.via.config;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Configuration;

/**
 * Hystrix related configuration.
 * 
 * @author Emoke_Borsi
 */
@Configuration
@EnableCircuitBreaker
@EnableHystrixDashboard
public class CircuitBreakerConfig {
}
