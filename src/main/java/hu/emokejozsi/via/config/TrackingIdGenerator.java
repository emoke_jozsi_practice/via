package hu.emokejozsi.via.config;

import java.util.UUID;

import org.springframework.stereotype.Component;

/**
 * This class is responsible for generating unique tracking ids.
 * 
 * @author Emoke_Borsi
 *
 */
@Component
public class TrackingIdGenerator {

    /**
     * Generates a new tracking id. This implementation uses
     * {@link UUID#randomUUID()}.
     * 
     * @return the unique tracking id
     */
    public String generateTrackingId() {
        return UUID.randomUUID().toString();
    }
}
