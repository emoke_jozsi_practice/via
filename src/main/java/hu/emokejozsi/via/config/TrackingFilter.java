package hu.emokejozsi.via.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

/**
 * Servlet filter that generates a new UUID (GUID) for every request, stores it
 * in the MDC of the logging framework and in the {@link ServletRequest} as an
 * attribute, both with a key named <code>guid</code>.
 * 
 * @author Emoke_Borsi
 *
 */
@Component // filters every request
public class TrackingFilter implements Filter {

    public static final String GUID = "guid";

    private final Logger logger;
    private final TrackingIdGenerator trackingIdGenerator;

    public TrackingFilter(Logger logger, TrackingIdGenerator trackingIdGenerator) {
        this.logger = logger;
        this.trackingIdGenerator = trackingIdGenerator;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String guid = trackingIdGenerator.generateTrackingId();
        logger.info("Generated guid: {}", guid);

        MDC.put(GUID, guid);
        request.setAttribute(GUID, guid);

        try {
            chain.doFilter(request, response);
        } finally {
            MDC.remove(GUID);
        }
    }

    @Override
    public void destroy() {
    }
}
