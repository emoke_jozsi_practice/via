package hu.emokejozsi.via.config;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Configuration for external HTTP calls.
 * 
 * @author Emoke_Borsi
 */
@Configuration
public class RestClientConfig {

    @Bean
    public HttpClientConnectionManager connectionManager(
            @Value("${hu.emokejozsi.via.users.httpconn.max-total}") int maxTotal,
            @Value("${hu.emokejozsi.via.users.httpconn.max-per-route}") int maxPerRoute,
            @Value("${hu.emokejozsi.via.users.httpconn.validate-after-inactivity}") int validateAfterInactivity) {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();

        connectionManager.setMaxTotal(maxTotal);
        connectionManager.setDefaultMaxPerRoute(maxPerRoute);

        // this is for evicting stale connections
        // other solution would be to have a dedicated thread for this
        connectionManager.setValidateAfterInactivity(validateAfterInactivity);

        return connectionManager;
    }

    @Bean
    public RequestConfig requestConfig(
            @Value("${hu.emokejozsi.via.users.httpconn.connect-timeout}") int connectTimeout,
            @Value("${hu.emokejozsi.via.users.httpconn.connection-request-timeout}") int connectionRequestTimeout,
            @Value("${hu.emokejozsi.via.users.httpconn.socket-timeout}") int socketTimeout) {
        return RequestConfig
                .custom()
                .setConnectTimeout(connectTimeout)
                .setConnectionRequestTimeout(connectionRequestTimeout)
                .setSocketTimeout(socketTimeout)
                .build();
    }

    @Bean
    public HttpClient httpClient(HttpClientConnectionManager connectionManager, RequestConfig requestConfig) {
        return HttpClientBuilder
                .create()
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig)
                .build();
    }

    @Bean
    public RestTemplate restTemplate(HttpClient httpClient, RestTemplateBuilder restTemplateBuilder) {
        // using the autoconfigured restTemplateBuilder to have
        // resttemplate metrics out-of-the-box
        return restTemplateBuilder.configure(new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpClient)));
    }
}
