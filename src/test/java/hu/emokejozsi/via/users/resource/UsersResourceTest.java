package hu.emokejozsi.via.users.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import hu.emokejozsi.via.users.facade.UserFacade;
import hu.emokejozsi.via.users.facade.model.UserWithAdditionalInfo;
import hu.emokejozsi.via.users.resource.model.ViaUser;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

@ExtendWith(MockitoExtension.class)
public class UsersResourceTest {

    private UsersResource underTest;

    @Mock
    private UserFacade mockUserFacade;
    @Mock
    private ViaUserConverter mockConverter;
    @Mock
    private Logger mockLogger;

    @BeforeEach
    public void setup() {
        underTest = new UsersResource(mockUserFacade, mockConverter, mockLogger);
    }

    @Test
    public void testGetUsersNonEmpty() {
        // GIVEN
        UserWithAdditionalInfo user = UserWithAdditionalInfo.builder().build();
        when(mockUserFacade.getAllUsers()).thenReturn(Arrays.asList(user));

        ViaUser viaUser = ViaUser.builder().build();
        when(mockConverter.convertToViaUser(any(UserWithAdditionalInfo.class))).thenReturn(viaUser);

        // WHEN
        ResponseEntity<List<ViaUser>> response = underTest.getAllUsers();

        // THEN
        checkOkResponse(response);

        List<ViaUser> viaUsers = response.getBody();
        assertThat(viaUsers, notNullValue());
        assertThat(viaUsers, hasSize(1));
        assertThat(viaUsers, contains(viaUser));

        verify(mockLogger).info(anyString());
        verify(mockLogger).debug(anyString(), eq(viaUsers));
    }

    @Test
    public void testGetUsersEmpty() {
        // GIVEN
        when(mockUserFacade.getAllUsers()).thenReturn(Collections.emptyList());

        // WHEN
        ResponseEntity<List<ViaUser>> response = underTest.getAllUsers();

        checkOkResponse(response);

        List<ViaUser> viaUsers = response.getBody();
        assertThat(viaUsers, notNullValue());
        assertThat(viaUsers, empty());

        verify(mockLogger).info(anyString());
        verify(mockLogger).debug(anyString(), eq(viaUsers));
    }

    @Test
    public void testGetUser() throws UserNotFoundException {
        // GIVEN
        UserWithAdditionalInfo user = UserWithAdditionalInfo.builder().build();
        when(mockUserFacade.getUser(5L)).thenReturn(user);

        ViaUser viaUser = ViaUser.builder().build();
        when(mockConverter.convertToViaUser(user)).thenReturn(viaUser);

        // WHEN
        ResponseEntity<ViaUser> response = underTest.getUser("5");

        // THEN
        checkOkResponse(response, HttpStatus.OK);

        ViaUser resultUser = response.getBody();
        assertThat(resultUser, notNullValue());
        assertThat(resultUser, is(viaUser));

        verify(mockLogger).info(anyString(), eq("5"));
        verify(mockLogger).debug(anyString(), eq(resultUser));
    }

    @Test
    public void testGetUserNotFound() throws UserNotFoundException {
        // GIVEN
        when(mockUserFacade.getUser(5L)).thenThrow(new UserNotFoundException(5L, ""));

        // WHEN - THEN
        assertThrows(UserNotFoundException.class, () -> underTest.getUser("5"));

        verify(mockLogger).info(anyString(), eq("5"));
    }

    @Test
    public void testCreateUser() throws UserNotFoundException {
        // GIVEN
        ViaUser viaUser = ViaUser.builder().build();
        UserWithAdditionalInfo user = UserWithAdditionalInfo.builder().build();
        when(mockConverter.convertToUserWithAdditionalInfo(viaUser)).thenReturn(user);

        UserWithAdditionalInfo createdUser = UserWithAdditionalInfo.builder().build();
        when(mockUserFacade.createUser(user)).thenReturn(createdUser);

        ViaUser createdViaUser = ViaUser.builder().withId(5L).build();
        when(mockConverter.convertToViaUser(createdUser)).thenReturn(createdViaUser);

        // WHEN
        ResponseEntity<ViaUser> response = underTest.createUser(viaUser);

        // THEN
        checkCreatedResponse(response);

        ViaUser resultUser = response.getBody();
        assertThat(resultUser, notNullValue());
        assertThat(resultUser, is(createdViaUser));

        verify(mockLogger).info(anyString(), eq(viaUser));
        verify(mockLogger).debug(anyString(), eq(resultUser));
    }

    @Test
    public void testUpdateUser() throws UserNotFoundException {
        // GIVEN
        ViaUser viaUser = ViaUser.builder().build();
        UserWithAdditionalInfo user = UserWithAdditionalInfo.builder().build();
        when(mockConverter.convertToUserWithAdditionalInfo(viaUser)).thenReturn(user);

        UserWithAdditionalInfo updatedUser = UserWithAdditionalInfo.builder().build();
        when(mockUserFacade.updateUser(user)).thenReturn(updatedUser);

        ViaUser updatedViaUser = ViaUser.builder().withId(5L).build();
        when(mockConverter.convertToViaUser(updatedUser)).thenReturn(updatedViaUser);

        // WHEN
        ResponseEntity<ViaUser> response = underTest.updateUser("5", viaUser);

        // THEN
        checkOkResponse(response);

        ViaUser resultUser = response.getBody();
        assertThat(resultUser, notNullValue());
        assertThat(resultUser, is(updatedViaUser));

        verify(mockLogger).info(anyString(), eq("5"), eq(viaUser));
        verify(mockLogger).debug(anyString(), eq(updatedViaUser));
    }

    @Test
    public void testUpdateUserNotFound() throws UserNotFoundException {
        // GIVEN
        ViaUser viaUser = ViaUser.builder().build();
        UserWithAdditionalInfo user = UserWithAdditionalInfo.builder().build();
        when(mockConverter.convertToUserWithAdditionalInfo(viaUser)).thenReturn(user);

        when(mockUserFacade.updateUser(user)).thenThrow(new UserNotFoundException(5L, ""));

        // WHEN - THEN
        assertThrows(UserNotFoundException.class, () -> underTest.updateUser("5", viaUser));

        verify(mockLogger).info(anyString(), eq("5"), eq(viaUser));
    }

    @Test
    public void testDeleteUser() throws UserNotFoundException {
        // GIVEN

        // WHEN
        ResponseEntity<?> response = underTest.deleteUser("5");

        // THEN
        checkNoContentResponse(response);

        verify(mockUserFacade).deleteUser(5L);

        verify(mockLogger).info(anyString(), eq("5"));
        verify(mockLogger).debug(anyString(), eq("5"));
    }

    private void checkOkResponse(ResponseEntity<?> response) {
        checkOkResponse(response, HttpStatus.OK);
    }

    private void checkCreatedResponse(ResponseEntity<?> response) {
        checkOkResponse(response, HttpStatus.CREATED);
    }

    private void checkOkResponse(ResponseEntity<?> response, HttpStatus status) {
        assertThat(response, notNullValue());
        assertThat(response.getStatusCode(), equalTo(status));
        assertThat(response.getHeaders().getContentType(), equalTo(MediaType.APPLICATION_JSON_UTF8));
        assertThat(response.getHeaders().getCacheControl(),
                both(containsString("max-age")).and(containsString("must-revalidate")));
    }

    private void checkNoContentResponse(ResponseEntity<?> response) {
        assertThat(response, notNullValue());
        assertThat(response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));
    }
}
