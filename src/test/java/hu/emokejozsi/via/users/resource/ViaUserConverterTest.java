package hu.emokejozsi.via.users.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hu.emokejozsi.via.users.facade.model.UserWithAdditionalInfo;
import hu.emokejozsi.via.users.resource.model.ViaUser;

public class ViaUserConverterTest {

    private ViaUserConverter underTest;

    @BeforeEach
    public void setup() {
        underTest = new ViaUserConverter();
    }

    @Test
    public void testConvertToUser() {
        // GIVEN
        ViaUser original = ViaUser.builder().withId(1L).withName("Name").withUsername("username").build();

        // WHEN
        UserWithAdditionalInfo result = underTest.convertToUserWithAdditionalInfo(original);

        // THEN
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(equalTo(original.getId())));
        assertThat(result.getName(), is(both(notNullValue()).and(equalTo(original.getName()))));
        assertThat(result.getUsername(), is(both(notNullValue()).and(equalTo(original.getUsername()))));
    }

    @Test
    public void testConvertToUserEmptyValues() {
        // GIVEN
        ViaUser original = ViaUser.builder().build();

        // WHEN
        UserWithAdditionalInfo result = underTest.convertToUserWithAdditionalInfo(original);

        // THEN
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(equalTo(original.getId())));
        assertThat(result.getName(), is(equalTo(original.getName())));
        assertThat(result.getUsername(), is(equalTo(original.getUsername())));
    }

    @Test
    public void testConvertToViaUser() {
        // GIVEN
        UserWithAdditionalInfo original = UserWithAdditionalInfo.builder().withId(1L).withName("Name")
                .withUsername("username").build();

        // WHEN
        ViaUser result = underTest.convertToViaUser(original);

        // THEN
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(equalTo(original.getId())));
        assertThat(result.getName(), is(both(notNullValue()).and(equalTo(original.getName()))));
        assertThat(result.getUsername(), is(both(notNullValue()).and(equalTo(original.getUsername()))));
    }

    @Test
    public void testConvertToViaUserEmptyValues() {
        // GIVEN
        UserWithAdditionalInfo original = UserWithAdditionalInfo.builder().build();

        // WHEN
        ViaUser result = underTest.convertToViaUser(original);

        // THEN
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(equalTo(original.getId())));
        assertThat(result.getName(), is(equalTo(original.getName())));
        assertThat(result.getUsername(), is(equalTo(original.getUsername())));
    }
}
