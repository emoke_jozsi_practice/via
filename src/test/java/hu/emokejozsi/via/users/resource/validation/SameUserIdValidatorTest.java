package hu.emokejozsi.via.users.resource.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hu.emokejozsi.via.users.resource.model.ViaUser;

public class SameUserIdValidatorTest {

    private SameUserIdValidator underTest;

    @BeforeEach
    public void setup() {
        underTest = new SameUserIdValidator();
    }

    @Test
    public void testIsValidTrue() {
        // GIVEN
        ViaUser user = ViaUser.builder().withId(5L).build();

        // WHEN
        boolean result = underTest.isValid(new Object[] { "5", user }, null);

        // THEN
        assertThat(result, is(true));
    }

    @Test
    public void testIsValidFalse() {
        // GIVEN
        ViaUser user = ViaUser.builder().withId(5L).build();

        // WHEN
        boolean result = underTest.isValid(new Object[] { "6", user }, null);

        // THEN
        assertThat(result, is(false));
    }

    @Test
    public void testIsValidNullParameter() {
        // GIVEN

        // WHEN - THEN
        assertThrows(IllegalArgumentException.class, () -> underTest.isValid(new Object[] { null, null }, null));
    }
}
