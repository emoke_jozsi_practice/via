package hu.emokejozsi.via.users.resource.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hu.emokejozsi.via.users.resource.model.ViaUser;

public class NullUserIdValidatorTest {

    private NullUserIdValidator underTest;

    @BeforeEach
    public void setup() {
        underTest = new NullUserIdValidator();
    }

    @Test
    public void testIsValidTrue() {
        // GIVEN
        ViaUser user = ViaUser.builder().build();

        // WHEN
        boolean result = underTest.isValid(user, null);

        // THEN
        assertThat(result, is(true));
    }

    @Test
    public void testIsValidFalse() {
        // GIVEN
        ViaUser user = ViaUser.builder().withId(5L).build();

        // WHEN
        boolean result = underTest.isValid(user, null);

        // THEN
        assertThat(result, is(false));
    }
}
