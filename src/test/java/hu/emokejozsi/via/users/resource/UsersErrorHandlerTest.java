package hu.emokejozsi.via.users.resource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import hu.emokejozsi.via.config.TrackingFilter;
import hu.emokejozsi.via.users.resource.model.ViaError;
import hu.emokejozsi.via.users.resource.model.ViaErrorResponse;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

@ExtendWith(MockitoExtension.class)
public class UsersErrorHandlerTest {

    private static final String PATH = "/users";
    private static final String GUID = "123456789";
    private static final String CACHE_NO_STORE = "no-store";

    private UsersErrorHandler underTest;

    @Mock
    private Logger mockLogger;
    @Mock
    private HttpServletRequest mockHttpServletRequest;

    private ServletWebRequest request;

    @BeforeEach
    public void setup() {
        underTest = new UsersErrorHandler(mockLogger);
        request = new ServletWebRequest(mockHttpServletRequest);
    }

    @Test
    public void testHandleInternalServerError() {
        // GIVEN
        when(mockHttpServletRequest.getRequestURI()).thenReturn(PATH);
        when(mockHttpServletRequest.getAttribute(TrackingFilter.GUID)).thenReturn(GUID);

        IllegalArgumentException exception = new IllegalArgumentException();

        // WHEN
        ResponseEntity<Object> response = underTest.handleInternalServerError(request,
                exception);

        // THEN
        checkErrorResponse(response, HttpStatus.INTERNAL_SERVER_ERROR, ViaError.INTERNAL_SERVER_ERROR);
        verify(mockLogger).error(anyString(), any(IllegalArgumentException.class));
    }

    @Test
    public void testHandleUserNotFound() {
        // GIVEN
        when(mockHttpServletRequest.getRequestURI()).thenReturn(PATH);
        when(mockHttpServletRequest.getAttribute(TrackingFilter.GUID)).thenReturn(GUID);

        UserNotFoundException exception = new UserNotFoundException(5L, "");

        // WHEN
        ResponseEntity<Object> response = underTest.handleUserNotFound(request, exception);

        // THEN
        checkErrorResponse(response, HttpStatus.NOT_FOUND, ViaError.USER_NOT_FOUND);
        verify(mockLogger).debug(anyString(), any(UserNotFoundException.class), eq(5L));
    }

    @Test
    public void testHandleValidationError() {
        // GIVEN
        when(mockHttpServletRequest.getRequestURI()).thenReturn(PATH);
        when(mockHttpServletRequest.getAttribute(TrackingFilter.GUID)).thenReturn(GUID);

        ConstraintViolation<?> mockConstraintViolation1 = mock(ConstraintViolation.class);
        ConstraintViolation<?> mockConstraintViolation2 = mock(ConstraintViolation.class);

        when(mockConstraintViolation1.getMessage()).thenReturn("a");
        when(mockConstraintViolation2.getMessage()).thenReturn("b");

        ConstraintViolationException exception = new ConstraintViolationException("c",
                Set.of(mockConstraintViolation1, mockConstraintViolation2));

        // WHEN
        ResponseEntity<Object> response = underTest.handleValidationError(request, exception);

        // THEN
        checkErrorResponse(response, HttpStatus.BAD_REQUEST, ViaError.INVALID_DATA);
        verify(mockLogger).debug(anyString(), any(ConstraintViolationException.class));
    }

    @Test
    public void testHandleValidationErrorEmptyConstraintViolations() {
        // GIVEN
        when(mockHttpServletRequest.getRequestURI()).thenReturn(PATH);
        when(mockHttpServletRequest.getAttribute(TrackingFilter.GUID)).thenReturn(GUID);

        ConstraintViolationException exception = new ConstraintViolationException("a", null);

        // WHEN
        ResponseEntity<Object> response = underTest.handleValidationError(request, exception);

        // THEN
        checkErrorResponse(response, HttpStatus.BAD_REQUEST, ViaError.INVALID_DATA);
        verify(mockLogger).debug(anyString(), any(ConstraintViolationException.class));
    }

    @Test
    public void testHandleMethodArgumentTypeMismatch() {
        // GIVEN
        when(mockHttpServletRequest.getRequestURI()).thenReturn(PATH);
        when(mockHttpServletRequest.getAttribute(TrackingFilter.GUID)).thenReturn(GUID);

        MethodArgumentTypeMismatchException exception = new MethodArgumentTypeMismatchException("4", Long.class, "id",
                null, null);

        // WHEN
        ResponseEntity<Object> response = underTest.handleMethodArgumentTypeMismatch(request, exception);

        // THEN
        checkErrorResponse(response, HttpStatus.BAD_REQUEST, ViaError.INVALID_DATA);
        verify(mockLogger).debug(anyString(), any(MethodArgumentTypeMismatchException.class));
    }

    @Test
    public void testHandleMethodArgumentTypeMismatchNullRequiredType() {
        // GIVEN
        when(mockHttpServletRequest.getRequestURI()).thenReturn(PATH);
        when(mockHttpServletRequest.getAttribute(TrackingFilter.GUID)).thenReturn(GUID);

        MethodArgumentTypeMismatchException exception = new MethodArgumentTypeMismatchException("4", null, "id",
                null, null);

        // WHEN
        ResponseEntity<Object> response = underTest.handleMethodArgumentTypeMismatch(request, exception);

        // THEN
        checkErrorResponse(response, HttpStatus.BAD_REQUEST, ViaError.INVALID_DATA);
        verify(mockLogger).debug(anyString(), any(MethodArgumentTypeMismatchException.class));
    }

    @Test
    public void testHandleMethodArgumentNotValid() {
        // GIVEN
        when(mockHttpServletRequest.getRequestURI()).thenReturn(PATH);
        when(mockHttpServletRequest.getAttribute(TrackingFilter.GUID)).thenReturn(GUID);

        BindingResult mockBindingResult = mock(BindingResult.class);
        when(mockBindingResult.getAllErrors())
                .thenReturn(Arrays.asList(new ObjectError("id", "invalid"), new ObjectError("name", "missing")));

        MethodArgumentNotValidException exception = new MethodArgumentNotValidException(null, mockBindingResult);

        // WHEN
        ResponseEntity<Object> response = underTest.handleMethodArgumentNotValid(exception, null, null, request);

        // THEN
        checkErrorResponse(response, HttpStatus.BAD_REQUEST, ViaError.INVALID_DATA);
        verify(mockLogger).debug(anyString(), any(MethodArgumentNotValidException.class));
    }

    private void checkErrorResponse(ResponseEntity<Object> response, HttpStatus status, ViaError error) {
        assertThat(response, notNullValue());
        assertThat(response.getStatusCode(), is(status));
        assertThat(response.getHeaders().getContentType(), equalTo(MediaType.APPLICATION_JSON_UTF8));
        assertThat(response.getHeaders().getCacheControl(), is(CACHE_NO_STORE));

        Object body = response.getBody();
        assertThat(body, instanceOf(ViaErrorResponse.class));

        ViaErrorResponse errorResponse = (ViaErrorResponse) body;
        assertThat(errorResponse.getError(), is(error));
        assertThat(errorResponse.getPath(), is(PATH));
        assertThat(errorResponse.getGuid(), is(GUID));
    }
}
