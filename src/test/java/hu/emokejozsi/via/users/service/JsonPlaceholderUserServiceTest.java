package hu.emokejozsi.via.users.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import hu.emokejozsi.via.users.service.model.JsonPlaceholderUser;
import hu.emokejozsi.via.users.service.model.User;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

@ExtendWith(MockitoExtension.class)
public class JsonPlaceholderUserServiceTest {

    private JsonPlaceholderUserService underTest;

    @Mock
    private JsonPlaceholderUserServiceClient mockServiceClient;
    @Mock
    private JsonPlaceholderUserConverter mockConverter;

    @BeforeEach
    public void setup() {
        underTest = new JsonPlaceholderUserService(mockServiceClient, mockConverter);
    }

    @Test
    public void testGetUsersNonEmpty() {
        // GIVEN
        JsonPlaceholderUser remoteUser = JsonPlaceholderUser.builder().build();
        User user = User.builder().build();
        when(mockServiceClient.getUsers()).thenReturn(Arrays.asList(remoteUser));
        when(mockConverter.convertToUser(remoteUser)).thenReturn(user);

        // WHEN
        List<User> result = underTest.getUsers();

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result, hasItem(user));
    }

    @Test
    public void testGetUsersEmpty() {
        // GIVEN
        when(mockServiceClient.getUsers()).thenReturn(Collections.emptyList());

        // WHEN
        List<User> result = underTest.getUsers();

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, empty());
    }

    @Test
    public void testGetUser() throws UserNotFoundException {
        // GIVEN
        JsonPlaceholderUser remoteUser = JsonPlaceholderUser.builder().build();
        User user = User.builder().build();
        when(mockServiceClient.getUser(5L)).thenReturn(remoteUser);
        when(mockConverter.convertToUser(remoteUser)).thenReturn(user);

        // WHEN
        User result = underTest.getUser(5L);

        // THEN
        assertThat(result, is(user));
    }

    @Test
    public void testGetUserNotFound() throws UserNotFoundException {
        // GIVEN
        when(mockServiceClient.getUser(5L)).thenThrow(new UserNotFoundException(5L, ""));

        // WHEN - THEN
        assertThrows(UserNotFoundException.class, () -> underTest.getUser(5L));
    }

    @Test
    public void testCreateUser() throws UserNotFoundException {
        // GIVEN
        User user = User.builder().build();
        JsonPlaceholderUser remoteUser = JsonPlaceholderUser.builder().build();
        when(mockConverter.convertToJsonPlaceholderUser(user)).thenReturn(remoteUser);

        JsonPlaceholderUser createdRemoteUser = JsonPlaceholderUser.builder().build();
        when(mockServiceClient.createUser(remoteUser)).thenReturn(createdRemoteUser);

        User createdUser = User.builder().build();
        when(mockConverter.convertToUser(createdRemoteUser)).thenReturn(createdUser);

        // WHEN
        User result = underTest.createUser(user);

        // THEN
        assertThat(result, is(createdUser));
    }

    @Test
    public void testUpdateUser() throws UserNotFoundException {
        // GIVEN
        User user = User.builder().withId(5L).build();
        JsonPlaceholderUser remoteUser = JsonPlaceholderUser.builder().withId(5L).build();
        when(mockConverter.convertToJsonPlaceholderUser(user)).thenReturn(remoteUser);

        // WHEN
        User result = underTest.updateUser(user);

        // THEN
        assertThat(result, is(user));
        verify(mockServiceClient).updateUser(remoteUser);
    }

    @Test
    public void testUpdateUserNotFound() throws UserNotFoundException {
        // GIVEN
        User user = User.builder().withId(5L).build();
        JsonPlaceholderUser remoteUser = JsonPlaceholderUser.builder().withId(5L).build();
        when(mockConverter.convertToJsonPlaceholderUser(user)).thenReturn(remoteUser);
        doThrow(new UserNotFoundException(5L, "")).when(mockServiceClient).updateUser(remoteUser);

        // WHEN - THEN
        assertThrows(UserNotFoundException.class, () -> underTest.updateUser(user));
    }

    @Test
    public void testDeleteUser() throws UserNotFoundException {
        // GIVEN

        // WHEN
        underTest.deleteUser(5L);

        // THEN
        verify(mockServiceClient).deleteUser(5L);
    }
}
