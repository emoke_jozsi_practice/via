package hu.emokejozsi.via.users.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import hu.emokejozsi.via.users.service.model.JsonPlaceholderUser;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

@ExtendWith(MockitoExtension.class)
public class JsonPlaceholderUserServiceClientTest {

    private static final String URL = "URL";

    private JsonPlaceholderUserServiceClient underTest;

    @Mock
    private RestTemplate mockRestTemplate;
    @Mock
    private Logger mockLogger;

    @BeforeEach
    public void setup() {
        underTest = new JsonPlaceholderUserServiceClient(mockRestTemplate, URL, mockLogger);
    }

    @Test
    public void testGetUsersNonEmpty() {
        // GIVEN
        JsonPlaceholderUser[] remoteResult = new JsonPlaceholderUser[] { JsonPlaceholderUser.builder().build() };
        when(mockRestTemplate.getForObject(URL, JsonPlaceholderUser[].class)).thenReturn(remoteResult);

        // WHEN
        List<JsonPlaceholderUser> result = underTest.getUsers();

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, hasSize(1));
        assertThat(result, hasItem(remoteResult[0]));
    }

    @Test
    public void testGetUsersEmpty() {
        // GIVEN
        JsonPlaceholderUser[] remoteResult = new JsonPlaceholderUser[] {};
        when(mockRestTemplate.getForObject(URL, JsonPlaceholderUser[].class)).thenReturn(remoteResult);

        // WHEN
        List<JsonPlaceholderUser> result = underTest.getUsers();

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, empty());
    }

    @Test
    public void testGetUsersNull() {
        // GIVEN
        when(mockRestTemplate.getForObject(URL, JsonPlaceholderUser[].class)).thenReturn(null);

        // WHEN
        List<JsonPlaceholderUser> result = underTest.getUsers();

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, empty());
    }

    @Test
    public void testGetUser() throws UserNotFoundException {
        // GIVEN
        JsonPlaceholderUser remoteResult = JsonPlaceholderUser.builder().build();
        when(mockRestTemplate.getForObject(URL + "/{id}", JsonPlaceholderUser.class, 5L)).thenReturn(remoteResult);

        // WHEN
        JsonPlaceholderUser result = underTest.getUser(5L);

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, is(remoteResult));
    }

    @Test
    public void testGetUserNull() throws UserNotFoundException {
        // GIVEN
        when(mockRestTemplate.getForObject(URL + "/{id}", JsonPlaceholderUser.class, 5L)).thenReturn(null);

        // WHEN
        UserNotFoundException exception = assertThrows(UserNotFoundException.class, () -> underTest.getUser(5L));

        // THEN
        assertThat(exception.getUserId(), is(5L));
    }

    @Test
    public void testGetUserNotFound() throws UserNotFoundException {
        // GIVEN
        when(mockRestTemplate.getForObject(URL + "/{id}", JsonPlaceholderUser.class, 5L))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

        // WHEN
        UserNotFoundException exception = assertThrows(UserNotFoundException.class, () -> underTest.getUser(5L));

        // THEN
        assertThat(exception.getUserId(), is(5L));
    }

    @Test
    public void testGetUserUnexpectedError() throws UserNotFoundException {
        // GIVEN
        when(mockRestTemplate.getForObject(URL + "/{id}", JsonPlaceholderUser.class, 5L))
                .thenThrow(new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR));

        // WHEN - THEN
        assertThrows(HttpClientErrorException.class, () -> underTest.getUser(5L));
    }

    @Test
    public void testCreateUser() throws UserNotFoundException {
        // GIVEN
        JsonPlaceholderUser user = JsonPlaceholderUser.builder().build();
        JsonPlaceholderUser remoteResult = JsonPlaceholderUser.builder().build();
        when(mockRestTemplate.postForObject(URL, user, JsonPlaceholderUser.class)).thenReturn(remoteResult);

        // WHEN
        JsonPlaceholderUser result = underTest.createUser(user);

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, is(remoteResult));
    }

    @Test
    public void testUpdateUser() throws UserNotFoundException {
        // GIVEN
        JsonPlaceholderUser user = JsonPlaceholderUser.builder().withId(5L).build();

        // WHEN
        underTest.updateUser(user);

        // THEN
        verify(mockRestTemplate).put(URL + "/{id}", user, 5L);
    }

    @Test
    public void testUpdateUserNotFound() throws UserNotFoundException {
        // GIVEN
        JsonPlaceholderUser user = JsonPlaceholderUser.builder().withId(5L).build();
        doThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND)).when(mockRestTemplate).put(URL + "/{id}", user, 5L);

        // WHEN
        UserNotFoundException exception = assertThrows(UserNotFoundException.class, () -> underTest.updateUser(user));

        // THEN
        assertThat(exception.getUserId(), is(5L));
    }

    @Test
    public void testUpdateUserUnexpectedException() throws UserNotFoundException {
        // GIVEN
        JsonPlaceholderUser user = JsonPlaceholderUser.builder().withId(5L).build();
        doThrow(new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR)).when(mockRestTemplate)
                .put(URL + "/{id}", user, 5L);

        // WHEN - THEN
        assertThrows(HttpClientErrorException.class, () -> underTest.updateUser(user));
    }

    @Test
    public void testDeleteUser() {
        // GIVEN

        // WHEN
        underTest.deleteUser(5L);

        // THEN
        verify(mockRestTemplate).delete(URL + "/{id}", 5L);
    }

    @Test
    public void testDeleteUserNotFound() throws UserNotFoundException {
        // GIVEN
        doThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND)).when(mockRestTemplate).delete(URL + "/{id}", 5L);

        // WHEN
        underTest.deleteUser(5L);

        // THEN
        verify(mockRestTemplate).delete(URL + "/{id}", 5L);
    }

    @Test
    public void testDeleteUserUnexpectedException() throws UserNotFoundException {
        // GIVEN
        doThrow(new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR)).when(mockRestTemplate)
                .delete(URL + "/{id}", 5L);

        // WHEN - THEN
        assertThrows(HttpClientErrorException.class, () -> underTest.deleteUser(5L));
    }
}
