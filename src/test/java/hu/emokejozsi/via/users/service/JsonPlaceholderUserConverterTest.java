package hu.emokejozsi.via.users.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hu.emokejozsi.via.users.service.JsonPlaceholderUserConverter;
import hu.emokejozsi.via.users.service.model.JsonPlaceholderUser;
import hu.emokejozsi.via.users.service.model.User;

public class JsonPlaceholderUserConverterTest {

    private JsonPlaceholderUserConverter underTest;

    @BeforeEach
    public void setup() {
        underTest = new JsonPlaceholderUserConverter();
    }

    @Test
    public void testConvertToUser() {
        // GIVEN
        JsonPlaceholderUser original = JsonPlaceholderUser.builder().withId(1L).withName("Name")
                .withUsername("username").build();

        // WHEN
        User result = underTest.convertToUser(original);

        // THEN
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(equalTo(original.getId())));
        assertThat(result.getName(), is(both(notNullValue()).and(equalTo(original.getName()))));
        assertThat(result.getUsername(), is(both(notNullValue()).and(equalTo(original.getUsername()))));
    }

    @Test
    public void testConvertToUserEmptyValues() {
        // GIVEN
        JsonPlaceholderUser original = JsonPlaceholderUser.builder().build();

        // WHEN
        User result = underTest.convertToUser(original);

        // THEN
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(equalTo(original.getId())));
        assertThat(result.getName(), is(equalTo(original.getName())));
        assertThat(result.getUsername(), is(equalTo(original.getUsername())));
    }

    @Test
    public void testConvertToJsonPlaceholderUser() {
        // GIVEN
        User original = User.builder().withId(1L).withName("Name").withUsername("username").build();

        // WHEN
        JsonPlaceholderUser result = underTest.convertToJsonPlaceholderUser(original);

        // THEN
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(equalTo(original.getId())));
        assertThat(result.getName(), is(both(notNullValue()).and(equalTo(original.getName()))));
        assertThat(result.getUsername(), is(both(notNullValue()).and(equalTo(original.getUsername()))));
    }

    @Test
    public void testConvertToJsonPlaceholderUserEmptyValues() {
        // GIVEN
        User original = User.builder().build();

        // WHEN
        JsonPlaceholderUser result = underTest.convertToJsonPlaceholderUser(original);

        // THEN
        assertThat(result, is(notNullValue()));
        assertThat(result.getId(), is(equalTo(original.getId())));
        assertThat(result.getName(), is(equalTo(original.getName())));
        assertThat(result.getUsername(), is(equalTo(original.getUsername())));
    }
}
