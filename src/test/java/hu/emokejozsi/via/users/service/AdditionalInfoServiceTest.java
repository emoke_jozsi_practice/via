package hu.emokejozsi.via.users.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;

import hu.emokejozsi.via.users.dao.UserAdditionalInfoRepository;
import hu.emokejozsi.via.users.dao.model.UserAdditionalInfo;
import hu.emokejozsi.via.users.service.model.AdditionalInfo;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

@ExtendWith(MockitoExtension.class)
public class AdditionalInfoServiceTest {

    private AdditionalInfoService underTest;

    @Mock
    private UserAdditionalInfoConverter mockConverter;
    @Mock
    private UserAdditionalInfoRepository mockRepo;
    @Mock
    private Logger mockLogger;

    @BeforeEach
    public void setup() {
        underTest = new AdditionalInfoService(mockConverter, mockRepo, mockLogger);
    }

    @Test
    public void testGetAdditionalInfos() {
        // GIVEN
        when(mockRepo.findAll())
                .thenReturn(Arrays.asList(UserAdditionalInfo.builder().build(), UserAdditionalInfo.builder().build()));

        AdditionalInfo info1 = AdditionalInfo.builder().build();
        AdditionalInfo info2 = AdditionalInfo.builder().build();
        when(mockConverter.convertToAdditionalInfo(any(UserAdditionalInfo.class)))
                .thenReturn(info1, info2);

        // WHEN
        List<AdditionalInfo> result = underTest.getAdditionalInfos();

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, hasSize(2));
        assertThat(result, contains(info1, info2));
    }

    @Test
    public void testGetAdditionalInfosEmpty() {
        // GIVEN
        when(mockRepo.findAll()).thenReturn(new ArrayList<>());

        // WHEN
        List<AdditionalInfo> result = underTest.getAdditionalInfos();

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, empty());
    }

    @Test
    public void testGetAdditionalInfo() throws UserNotFoundException {
        // GIVEN
        UserAdditionalInfo userAdditionalInfo = UserAdditionalInfo.builder().build();
        when(mockRepo.findById(5L)).thenReturn(Optional.ofNullable(userAdditionalInfo));

        AdditionalInfo info = AdditionalInfo.builder().build();
        when(mockConverter.convertToAdditionalInfo(userAdditionalInfo)).thenReturn(info);

        // WHEN
        Optional<AdditionalInfo> result = underTest.getAdditionalInfo(5L);

        // THEN
        assertThat(result.isPresent(), is(true));
        assertThat(result.get(), is(info));
    }

    @Test
    public void testGetAdditionalInfoNotFound() throws UserNotFoundException {
        // GIVEN
        when(mockRepo.findById(5L)).thenReturn(Optional.empty());

        // WHEN
        Optional<AdditionalInfo> result = underTest.getAdditionalInfo(5L);

        // THEN
        assertThat(result.isPresent(), is(false));
    }

    @Test
    public void testSaveAdditionalInfo() throws UserNotFoundException {
        // GIVEN
        AdditionalInfo info = AdditionalInfo.builder().build();
        UserAdditionalInfo userAdditionalInfo = UserAdditionalInfo.builder().build();
        when(mockConverter.convertToUserAdditionalInfo(info)).thenReturn(userAdditionalInfo);

        // WHEN
        underTest.saveAdditionalInfo(info);

        // THEN
        verify(mockRepo).save(userAdditionalInfo);
    }

    @Test
    public void testDeleteAdditionalInfo() throws UserNotFoundException {
        // GIVEN

        // WHEN
        underTest.deleteAdditionalInfo(5L);

        // THEN
        verify(mockRepo).deleteById(5L);
    }

    @Test
    public void testDeleteAdditionalInfoNotFound() throws UserNotFoundException {
        // GIVEN
        doThrow(new EmptyResultDataAccessException(1)).when(mockRepo).deleteById(5L);

        // WHEN
        underTest.deleteAdditionalInfo(5L);

        // THEN
        verify(mockRepo).deleteById(5L);
    }
}
