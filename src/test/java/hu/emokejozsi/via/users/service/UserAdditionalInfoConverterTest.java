package hu.emokejozsi.via.users.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hu.emokejozsi.via.users.dao.model.UserAdditionalInfo;
import hu.emokejozsi.via.users.service.model.AdditionalInfo;

public class UserAdditionalInfoConverterTest {

    private UserAdditionalInfoConverter underTest;

    @BeforeEach
    public void setup() {
        underTest = new UserAdditionalInfoConverter();
    }

    @Test
    public void testConvertToUserAdditionalInfo() {
        // GIVEN
        AdditionalInfo original = AdditionalInfo.builder()
                .withUserId(1L)
                .withDirectMarketingEnabled(true)
                .build();

        // WHEN
        UserAdditionalInfo result = underTest.convertToUserAdditionalInfo(original);

        // THEN
        check(result, original);
    }

    @Test
    public void testConvertToUserAdditionalInfoEmptyValues() {
        // GIVEN
        AdditionalInfo original = AdditionalInfo.builder().build();

        // WHEN
        UserAdditionalInfo result = underTest.convertToUserAdditionalInfo(original);

        // THEN
        check(result, original);
    }

    @Test
    public void testConvertToAdditionalInfo() {
        // GIVEN
        UserAdditionalInfo original = UserAdditionalInfo.builder()
                .withUserId(1L)
                .withDirectMarketingEnabled(true)
                .build();

        // WHEN
        AdditionalInfo result = underTest.convertToAdditionalInfo(original);

        // THEN
        check(result, original);
    }

    @Test
    public void testConvertToAdditionalInfoEmptyValues() {
        // GIVEN
        UserAdditionalInfo original = UserAdditionalInfo.builder().build();

        // WHEN
        AdditionalInfo result = underTest.convertToAdditionalInfo(original);

        // THEN
        check(result, original);
    }

    private void check(UserAdditionalInfo result, AdditionalInfo original) {
        assertThat(result, is(notNullValue()));
        assertThat(result.getUserId(), is(equalTo(original.getUserId())));
        assertThat(result.isDirectMarketingEnabled(), is(original.isDirectMarketingEnabled()));
    }

    private void check(AdditionalInfo result, UserAdditionalInfo original) {
        assertThat(result, is(notNullValue()));
        assertThat(result.getUserId(), is(equalTo(original.getUserId())));
        assertThat(result.isDirectMarketingEnabled(), is(original.isDirectMarketingEnabled()));
    }
}
