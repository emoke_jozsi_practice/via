package hu.emokejozsi.via.users.facade;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import hu.emokejozsi.via.users.facade.model.UserWithAdditionalInfo;
import hu.emokejozsi.via.users.service.AdditionalInfoService;
import hu.emokejozsi.via.users.service.JsonPlaceholderUserService;
import hu.emokejozsi.via.users.service.model.AdditionalInfo;
import hu.emokejozsi.via.users.service.model.User;
import hu.emokejozsi.via.users.service.model.UserNotFoundException;

@ExtendWith(MockitoExtension.class)
public class UserFacadeTest {

    private UserFacade underTest;

    @Mock
    private JsonPlaceholderUserService mockUserService;
    @Mock
    private AdditionalInfoService mockAdditionalInfoService;
    @Mock
    private UserWithAdditionalInfoConverter mockConverter;

    @BeforeEach
    public void setup() {
        underTest = new UserFacade(mockUserService, mockAdditionalInfoService, mockConverter);
    }

    @Test
    public void testGetAllUsersEmpty() {
        // GIVEN
        when(mockUserService.getUsers()).thenReturn(new ArrayList<>());
        when(mockAdditionalInfoService.getAdditionalInfos()).thenReturn(new ArrayList<>());

        // WHEN
        List<UserWithAdditionalInfo> result = underTest.getAllUsers();

        // THEN
        assertNotNull(result);
        assertThat(result, empty());
    }

    @Test
    public void testGetAllUsersNonEmpty() {
        // GIVEN
        User user1 = User.builder().withId(2L).build();
        User user2 = User.builder().withId(3L).build();
        when(mockUserService.getUsers()).thenReturn(Arrays.asList(user1, user2));

        AdditionalInfo additionalInfo1 = AdditionalInfo.builder()
                .withUserId(2L)
                .withDirectMarketingEnabled(true)
                .build();
        when(mockAdditionalInfoService.getAdditionalInfos()).thenReturn(Arrays.asList(additionalInfo1));

        UserWithAdditionalInfo userWithInfo1 = UserWithAdditionalInfo.builder().withId(2L).build();
        UserWithAdditionalInfo userWithInfo2 = UserWithAdditionalInfo.builder().withId(3L).build();
        when(mockConverter.convertToUserWithAdditionalInfo(any(User.class), any()))
                .thenReturn(userWithInfo1, userWithInfo2);

        // WHEN
        List<UserWithAdditionalInfo> result = underTest.getAllUsers();

        // THEN
        assertNotNull(result);
        assertThat(result, hasSize(2));

        assertThat(result, contains(userWithInfo1, userWithInfo2));

        verify(mockConverter).convertToUserWithAdditionalInfo(user1, additionalInfo1);
        verify(mockConverter).convertToUserWithAdditionalInfo(user2, null);
    }

    @Test
    public void testGetUser() throws UserNotFoundException {
        // GIVEN
        User user = User.builder().withId(5L).build();
        when(mockUserService.getUser(5)).thenReturn(user);

        AdditionalInfo additionalInfo = AdditionalInfo.builder().withUserId(5L).build();
        when(mockAdditionalInfoService.getAdditionalInfo(5)).thenReturn(Optional.ofNullable(additionalInfo));

        UserWithAdditionalInfo userWithInfo = UserWithAdditionalInfo.builder().withId(5L).build();
        when(mockConverter.convertToUserWithAdditionalInfo(user, additionalInfo))
                .thenReturn(userWithInfo);

        // WHEN
        UserWithAdditionalInfo result = underTest.getUser(5);

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, is(userWithInfo));
    }

    @Test
    public void testGetUserNotFound() throws UserNotFoundException {
        // GIVEN
        when(mockUserService.getUser(5)).thenThrow(new UserNotFoundException(5, ""));

        // WHEN
        UserNotFoundException exception = assertThrows(UserNotFoundException.class, () -> underTest.getUser(5));

        // THEN
        assertThat(exception.getUserId(), is(5L));
    }

    @Test
    public void testCreateUser() {
        // GIVEN
        UserWithAdditionalInfo userWithInfo = UserWithAdditionalInfo.builder().build();
        User user = User.builder().build();
        when(mockConverter.convertToUser(userWithInfo)).thenReturn(user);

        User createdUser = User.builder().withId(5L).build();
        when(mockUserService.createUser(user)).thenReturn(createdUser);

        AdditionalInfo additionalInfo = AdditionalInfo.builder().withUserId(5L).build();
        when(mockConverter.convertToAdditionalInfoWithNewId(userWithInfo, 5L)).thenReturn(additionalInfo);

        UserWithAdditionalInfo createdUserWithInfo = UserWithAdditionalInfo.builder().build();
        when(mockConverter.convertToUserWithAdditionalInfo(createdUser, additionalInfo))
                .thenReturn(createdUserWithInfo);

        // WHEN
        UserWithAdditionalInfo result = underTest.createUser(userWithInfo);

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, is(createdUserWithInfo));

        verify(mockAdditionalInfoService).saveAdditionalInfo(additionalInfo);
    }

    @Test
    public void testUpdateUser() throws UserNotFoundException {
        // GIVEN
        UserWithAdditionalInfo userWithInfo = UserWithAdditionalInfo.builder().withId(5L).build();
        User user = User.builder().withId(5L).build();
        when(mockConverter.convertToUser(userWithInfo)).thenReturn(user);

        User updatedUser = User.builder().withId(5L).build();
        when(mockUserService.updateUser(user)).thenReturn(updatedUser);

        AdditionalInfo additionalInfo = AdditionalInfo.builder().withUserId(5L).build();
        when(mockConverter.convertToAdditionalInfo(userWithInfo)).thenReturn(additionalInfo);

        // WHEN
        UserWithAdditionalInfo result = underTest.updateUser(userWithInfo);

        // THEN
        assertThat(result, notNullValue());
        assertThat(result, is(userWithInfo));

        verify(mockAdditionalInfoService).saveAdditionalInfo(additionalInfo);
    }

    @Test
    public void testUpdateUserNotFound() throws UserNotFoundException {
        // GIVEN
        UserWithAdditionalInfo userWithInfo = UserWithAdditionalInfo.builder().withId(5L).build();
        User user = User.builder().withId(5L).build();
        when(mockConverter.convertToUser(userWithInfo)).thenReturn(user);

        when(mockUserService.updateUser(user)).thenThrow(new UserNotFoundException(5, ""));

        // WHEN
        UserNotFoundException exception = assertThrows(UserNotFoundException.class,
                () -> underTest.updateUser(userWithInfo));

        // THEN
        assertThat(exception.getUserId(), is(5L));
    }

    @Test
    public void testDeleteUser() {
        // GIVEN

        // WHEN
        underTest.deleteUser(5L);

        // THEN
        verify(mockUserService).deleteUser(5L);
        verify(mockAdditionalInfoService).deleteAdditionalInfo(5L);
    }
}
