package hu.emokejozsi.via.users.facade;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hu.emokejozsi.via.users.facade.model.UserWithAdditionalInfo;
import hu.emokejozsi.via.users.service.model.AdditionalInfo;
import hu.emokejozsi.via.users.service.model.User;

public class UserWithAdditionalInfoConverterTest {

    private UserWithAdditionalInfoConverter underTest;

    @BeforeEach
    public void setup() {
        underTest = new UserWithAdditionalInfoConverter();
    }

    @Test
    public void testConvertToUserWithAdditionalInfo() {
        // GIVEN
        User user = User.builder()
                .withId(5L)
                .withName("Name")
                .withUsername("username")
                .build();
        AdditionalInfo additionalInfo = AdditionalInfo.builder()
                .withUserId(5L)
                .withDirectMarketingEnabled(true)
                .build();

        // WHEN
        UserWithAdditionalInfo result = underTest.convertToUserWithAdditionalInfo(user, additionalInfo);

        // THEN
        check(result, user, additionalInfo);
    }

    @Test
    public void testConvertToUserWithAdditionalInfoNull() {
        // GIVEN
        User user = User.builder()
                .withId(5L)
                .withName("Name")
                .withUsername("username")
                .build();

        // WHEN
        UserWithAdditionalInfo result = underTest.convertToUserWithAdditionalInfo(user, null);

        // THEN
        check(result, user, null);
    }

    @Test
    public void testConvertToUserWithAdditionalInfoEmptyValues() {
        // GIVEN
        User user = User.builder().build();

        // WHEN
        UserWithAdditionalInfo result = underTest.convertToUserWithAdditionalInfo(user, null);

        // THEN
        check(result, user, null);
    }

    @Test
    public void testConvertToUser() {
        // GIVEN
        UserWithAdditionalInfo userWithAdditionalInfo = UserWithAdditionalInfo.builder()
                .withId(5L)
                .withName("Name")
                .withUsername("username")
                .build();

        // WHEN
        User result = underTest.convertToUser(userWithAdditionalInfo);

        // THEN
        check(result, userWithAdditionalInfo);
    }

    @Test
    public void testConvertToUserEmptyValues() {
        // GIVEN
        UserWithAdditionalInfo userWithAdditionalInfo = UserWithAdditionalInfo.builder().build();

        // WHEN
        User result = underTest.convertToUser(userWithAdditionalInfo);

        // THEN
        check(result, userWithAdditionalInfo);
    }

    @Test
    public void testConvertToAdditionalInfo() {
        // GIVEN
        UserWithAdditionalInfo userWithAdditionalInfo = UserWithAdditionalInfo.builder()
                .withId(5L)
                .withDirectMarketingEnabled(true)
                .build();

        // WHEN
        AdditionalInfo result = underTest.convertToAdditionalInfo(userWithAdditionalInfo);

        // THEN
        check(result, userWithAdditionalInfo);
    }

    @Test
    public void testConvertToAdditionalInfoEmptyValues() {
        // GIVEN
        UserWithAdditionalInfo userWithAdditionalInfo = UserWithAdditionalInfo.builder().build();

        // WHEN
        AdditionalInfo result = underTest.convertToAdditionalInfo(userWithAdditionalInfo);

        // THEN
        check(result, userWithAdditionalInfo);
    }

    @Test
    public void testConvertToAdditionalInfoWithNewId() {
        // GIVEN
        UserWithAdditionalInfo userWithAdditionalInfo = UserWithAdditionalInfo.builder()
                .withDirectMarketingEnabled(true)
                .build();

        // WHEN
        AdditionalInfo result = underTest.convertToAdditionalInfoWithNewId(userWithAdditionalInfo, 5L);

        // THEN
        check(result, userWithAdditionalInfo, 5L);
    }

    private void check(UserWithAdditionalInfo result, User user, AdditionalInfo additionalInfo) {
        assertThat(result, notNullValue());
        assertThat(result.getId(), is(user.getId()));
        assertThat(result.getName(), is(user.getName()));
        assertThat(result.getUsername(), is(user.getUsername()));
        assertThat(result.isDirectMarketingEnabled(),
                is(Objects.nonNull(additionalInfo) ? additionalInfo.isDirectMarketingEnabled() : false));
    }

    private void check(User result, UserWithAdditionalInfo userWithInfo) {
        assertThat(result, notNullValue());
        assertThat(result.getId(), is(userWithInfo.getId()));
        assertThat(result.getName(), is(userWithInfo.getName()));
        assertThat(result.getUsername(), is(userWithInfo.getUsername()));
    }

    private void check(AdditionalInfo result, UserWithAdditionalInfo userWithAdditionalInfo) {
        assertThat(result, notNullValue());
        assertThat(result.getUserId(), is(userWithAdditionalInfo.getId()));
        assertThat(result.isDirectMarketingEnabled(), is(userWithAdditionalInfo.isDirectMarketingEnabled()));
    }

    private void check(AdditionalInfo result, UserWithAdditionalInfo userWithAdditionalInfo, long newId) {
        assertThat(result, notNullValue());
        assertThat(result.getUserId(), is(newId));
        assertThat(result.isDirectMarketingEnabled(), is(userWithAdditionalInfo.isDirectMarketingEnabled()));
    }
}
