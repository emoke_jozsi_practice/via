package hu.emokejozsi.via.it;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 8182)
@TestPropertySource(
        locations = "/test.properties",
        properties = "hu.emokejozsi.via.users.url=http://localhost:8182/usersRemoteEmpty")
public class ViaApplicationEmptyResponseTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetUsersEmptyResponse() throws Exception {
        // GIVEN

        // WHEN
        mockMvc.perform(get("/users"))
                // THEN
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(header().string(HttpHeaders.CACHE_CONTROL,
                        both(containsString("max-age")).and(containsString("must-revalidate"))))
                .andExpect(content().json("[]"));
    }
}
