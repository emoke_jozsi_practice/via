package hu.emokejozsi.via.it;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import hu.emokejozsi.via.users.resource.model.ViaError;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 8181)
@TestPropertySource("/test.properties")
public class ViaApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetUsers() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(get("/users"));
        // THEN
        validateOkResponse(result)
                .andExpect(jsonPath("$[0].id", is(1)));
    }

    @Test
    public void testGetUser() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(get("/users/5"));
        // THEN
        validateOkResponse(result)
                .andExpect(jsonPath("$.id", is(5)));
    }

    @Test
    public void testGetUserNotFound() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(get("/users/56"));
        // THEN
        validateNotFoundResponse(result, "/users/56");
    }

    @Test
    public void testGetUserIdNegativeNumber() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(get("/users/-6"));
        // THEN
        validateInvalidInputResponse(result, "/users/-6");
    }

    @Test
    public void testGetUserIdInvalidNumber() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(get("/users/ehh"));
        // THEN
        validateInvalidInputResponse(result, "/users/ehh");
    }

    @Test
    public void testPostUser() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\": \"My User\", \"username\": \"my_user\", \"directMarketingEnabled\": \"true\"}"));
        // THEN
        validateCreatedResponse(result)
                .andExpect(jsonPath("$.id", is(11)));
    }

    @Test
    public void testPostUserRequiredPropertyMissing() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"username\": \"my_user\", \"directMarketingEnabled\": \"true\"}"));
        // THEN
        validateInvalidInputResponse(result, "/users/");
    }

    @Test
    public void testPostUserIdSet() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(post("/users/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        "{\"id\": 5, \"name\": \"My User\", \"username\": \"my_user\", \"directMarketingEnabled\": \"true\"}"));
        // THEN
        validateInvalidInputResponse(result, "/users/");
    }

    @Test
    public void testPutUser() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(put("/users/5")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        "{\"id\": 5, \"name\": \"My User\", \"username\": \"my_user\", \"directMarketingEnabled\": \"true\"}"));
        // THEN
        validateOkResponse(result)
                .andExpect(jsonPath("$.id", is(5)));
    }

    @Test
    public void testPutUserIdMismatch() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(put("/users/5")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        "{\"id\": 6, \"name\": \"My User\", \"username\": \"my_user\", \"directMarketingEnabled\": \"true\"}"));
        // THEN
        validateInvalidInputResponse(result, "/users/5");
    }

    @Test
    public void testPutUserIdMissingInBody() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(put("/users/5")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        "{\"name\": \"My User\", \"username\": \"my_user\", \"directMarketingEnabled\": \"true\"}"));
        // THEN
        validateInvalidInputResponse(result, "/users/5");
    }

    @Test
    public void testPutUserRequiredPropertyMissing() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(put("/users/5")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"id\": 5, \"directMarketingEnabled\": \"true\"}"));
        // THEN
        validateInvalidInputResponse(result, "/users/5");
    }

    @Test
    public void testPutUserNotFound() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(put("/users/52")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(
                        "{\"id\": 52, \"name\": \"My User\", \"username\": \"my_user\", \"directMarketingEnabled\": \"true\"}"));
        // THEN
        validateNotFoundResponse(result, "/users/52");
    }

    @Test
    public void testDeleteUser() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(delete("/users/5"));
        // THEN
        validateNoContentResponse(result);
    }

    @Test
    public void testDeleteUserNotFound() throws Exception {
        // GIVEN
        // WHEN
        ResultActions result = mockMvc.perform(delete("/users/53"));
        // THEN
        validateNoContentResponse(result);
    }

    private ResultActions validateOkResponse(ResultActions result) throws Exception {
        return result.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(header().string(HttpHeaders.CACHE_CONTROL,
                        both(containsString("max-age")).and(containsString("must-revalidate"))));
    }

    private void validateNotFoundResponse(ResultActions result, String path) throws Exception {
        result.andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(header().string(HttpHeaders.CACHE_CONTROL, is("no-store")))
                .andExpect(jsonPath("$.path", is(path)))
                .andExpect(jsonPath("$.error.httpStatus", is(ViaError.USER_NOT_FOUND.getHttpStatus().name())))
                .andExpect(jsonPath("$.error.message", is(ViaError.USER_NOT_FOUND.getMessage())))
                .andExpect(jsonPath("$.guid", not(isEmptyOrNullString())));
    }

    private void validateInvalidInputResponse(ResultActions result, String path) throws Exception {
        result.andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(header().string(HttpHeaders.CACHE_CONTROL, is("no-store")))
                .andExpect(jsonPath("$.path", is(path)))
                .andExpect(jsonPath("$.error.httpStatus", is(ViaError.INVALID_DATA.getHttpStatus().name())))
                .andExpect(jsonPath("$.error.message", is(ViaError.INVALID_DATA.getMessage())))
                .andExpect(jsonPath("$.guid", not(isEmptyOrNullString())));
    }

    private ResultActions validateCreatedResponse(ResultActions result) throws Exception {
        return result.andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(header().string(HttpHeaders.CACHE_CONTROL,
                        both(containsString("max-age")).and(containsString("must-revalidate"))));
    }

    private void validateNoContentResponse(ResultActions result) throws Exception {
        result.andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }
}
