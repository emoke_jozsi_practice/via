package hu.emokejozsi.via.it;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import hu.emokejozsi.via.users.resource.model.ViaError;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 8183)
@TestPropertySource(
        locations = "/test.properties",
        properties = "hu.emokejozsi.via.users.url=http://localhost:8183/usersRemoteInvalid")
public class ViaApplicationRuntimeExceptionTest {

    private static final String CACHE_NO_STORE = "no-store";
    private static final String USERS_PATH = "/users";

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetUsersRuntimeException() throws Exception {
        // GIVEN

        // WHEN
        mockMvc.perform(get(USERS_PATH))
                // THEN
                .andExpect(status().is5xxServerError())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(header().string(HttpHeaders.CACHE_CONTROL, is(CACHE_NO_STORE)))
                .andExpect(jsonPath("$.path", is(USERS_PATH)))
                .andExpect(jsonPath("$.error.httpStatus", is(ViaError.INTERNAL_SERVER_ERROR.getHttpStatus().name())))
                .andExpect(jsonPath("$.error.message", is(ViaError.INTERNAL_SERVER_ERROR.getMessage())))
                .andExpect(jsonPath("$.guid", not(isEmptyOrNullString())));
    }
}
