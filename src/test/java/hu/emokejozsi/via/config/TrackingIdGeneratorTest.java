package hu.emokejozsi.via.config;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hu.emokejozsi.via.config.TrackingIdGenerator;

public class TrackingIdGeneratorTest {

    private static final int NUMBER_OF_RUNS = 10000;

    private TrackingIdGenerator underTest;

    @BeforeEach
    public void setup() {
        underTest = new TrackingIdGenerator();
    }

    @Test
    public void testGenerateTrackingIdUnique() {
        // GIVEN

        // WHEN
        List<String> guids = IntStream.range(0, NUMBER_OF_RUNS).mapToObj(i -> underTest.generateTrackingId()).distinct()
                .collect(toList());

        // THEN
        int expectedCount = (int) guids.stream().distinct().count();
        assertThat(expectedCount, is(NUMBER_OF_RUNS));
    }
}
