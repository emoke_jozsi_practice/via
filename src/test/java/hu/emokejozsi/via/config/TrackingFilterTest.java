package hu.emokejozsi.via.config;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import hu.emokejozsi.via.config.TrackingFilter;
import hu.emokejozsi.via.config.TrackingIdGenerator;

@ExtendWith(MockitoExtension.class)
public class TrackingFilterTest {

    private static final String GUID = "12345";

    private TrackingFilter underTest;

    @Mock
    private Logger logger;
    @Mock
    private ServletRequest mockServletRequest;
    @Mock
    private ServletResponse mockServletResponse;
    @Mock
    private FilterChain mockFilterChain;
    @Mock
    private TrackingIdGenerator mockTrackingIdGenerator;

    @BeforeEach
    public void setup() {
        underTest = new TrackingFilter(logger, mockTrackingIdGenerator);
    }

    @Test
    public void testDoFilter() throws IOException, ServletException {
        // GIVEN
        when(mockTrackingIdGenerator.generateTrackingId()).thenReturn(GUID);

        // WHEN
        underTest.doFilter(mockServletRequest, mockServletResponse, mockFilterChain);

        // THEN
        verify(mockServletRequest).setAttribute(TrackingFilter.GUID, GUID);
        verify(mockFilterChain).doFilter(mockServletRequest, mockServletResponse);
    }
}
