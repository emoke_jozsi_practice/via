# VIA - practice application

VIA (acronym for Very Interesting Application) is a sample REST based microservice. The main purpose of its creation is practising up-to-date tools and technologies, hence the business logic is very simple.

---

## What it does

VIA provides standard GET, POST, PUT and DELETE Rest endpoints to view and manipulate user data. Exchange format is JSON.

**Try it out:** [Swagger UI on Heroku](https://hu-emokejozsi-via.herokuapp.com/swagger-ui.html)

Behind the scenes, user data are stored in and read from a free stub backend service: [JsonPlaceholder](https://jsonplaceholder.typicode.com/). The user data managed by VIA is a simplified version
of the backend model (only id, name and username are in use at the moment.)

VIA sample app also stores additional user data (directMarketingEnabled) on a per user basis in its own database (only an in-memory database at this stage, for practicing purposes).

---

## Technical details

### Java version
* Java 10
* Java 8 streams, lambdas/method references, optionals are used where possible

### Spring
* Spring Boot 2 with embedded Tomcat
* Spring config through annotations, constructor injection
* Layers: resource - facade - service - dao
* Spring profiles: dev and prod
* Actuator endpoints enabled for dev profile
* Spring MVC
    * RestController methods with standard http status codes, cache-control header
    * RestTemplate for backend service calls (connection pool is configured for HttpClient)
    * error handling (`@ControllerAdvice`, `ResponseEntityExceptionHandler`, `@ExceptionHandler` methods)
* Spring Data JPA (`CrudRepository` + entity class) - only in-memory database (h2 with console enabled)

### Additional
* Validating input with JSR303 (hibernate) - annotations + custom annotation
* Tracking: Slf4j with Logback (logback-spring.xml, console and rolling file appenders, per profile config, guid generated in servlet filter, added to the request and to mdc)
* Hystrix (with hystrix dashboard enabled)
* `@Nonnull`, `@Nullable` (jsr305)
* Swagger for documentation (springfox, annotations and Docket bean)
* Very basic metrics with micrometer + graphite + grafana (no automated startup available, not accessible in the heroku deploy)

### Development and build tools
* Maven for dependency management and automated build&deploy
* Eclipse
    * plugins: maven, spring, sparkbuilder, spotbugs
	* built-in equals/hashcode/toString generation
* Git + BitBucket
* CI & CD: a simple pipeline on BitBucket (bitbucket-pipelines.yml) with manual step to deploy to Heroku (had to specify java 10 version)
* Dockerfile (created manually), dockerfile-maven plugin for building the image (not running as part of the standard build process)

### Testing
* unit and integration tests
* jUnit5 (had to exclude junit4, use latest surefire maven plugin and `@ExtendWith`)
* Mocking with Mockito and Wiremock (`@AutoConfigureWireMock`) + h2 in-memory db
* Hamcrest for assertions
* `@SpringBootTest` and `MockMvc` in integration tests
* JaCoCo for code coverage

### Possible extension points:
* extending monitoring, e.g. using `Timer`, `Gauge`, etc.
* security (Spring Security + JWT + Oauth 2.0)
* using real db (even maybe cassandra)
* caching (JPA entities + backend service responses) - combined with hystrix fallback mechanism
* splitting to 2 or 3 microservices (ribbon + eureka/consul/zookeeper OR kubernetes)
* config server
* log aggregation